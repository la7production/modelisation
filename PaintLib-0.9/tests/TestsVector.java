import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.la7prod.paintlib.geometry.Vector3D;

public class TestsVector {
	
	@Test
	public void testVecteurNormal() {
		Vector3D v = new Vector3D(1,2,3);
		assertEquals((int)v.scale(), 3);
	}
	
	@Test
	public void testVecteurUnitaire() {
		Vector3D v = new Vector3D(1,2,3);
		Vector3D u = v.normalize();
		double norme = v.scale();
		assertEquals((int)u.getX(), (int)(v.getX()/norme));
		assertEquals((int)u.getY(), (int)(v.getY()/norme));
		assertEquals((int)u.getZ(), (int)(v.getZ()/norme));
	}

	@Test
	public void testTranslation() {
		Vector3D v = new Vector3D(1,2,3);
		v.translate(new Vector3D(4,5,6));
		assertEquals((int)v.getX(), 5);
		assertEquals((int)v.getY(), 7);
		assertEquals((int)v.getZ(), 9);
	}
	
	@Test
	public void testAngleVecteurs() {
		Vector3D u = new Vector3D(1,1,0);
		Vector3D v = new Vector3D(1,0,0);
		System.out.println("cos(u,v)=" + Math.acos(Vector3D.cos(u, v)) + "\t" + Math.PI/4);
	}
	
	@Test
	public void testAngleX() {
		Vector3D v = new Vector3D(1,1,0);
		System.out.println("cos(u,x)=" + v.getAngleX() + "\t" + Math.PI/4);
	}
	
	@Test
	public void testRotationX() {
		
	}
	
	@Test
	public void testRotationY() {
		
	}
	
	@Test
	public void testRotationZ() {
		
	}

}
