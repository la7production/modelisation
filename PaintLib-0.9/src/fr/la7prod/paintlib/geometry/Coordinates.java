/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.geometry;

/**
 * Coordinates possède 3 composantes x y et z permettant de déterminer
 * un endroit précis de l'espace.
 * 
 * Point3D et Vector3D sont des enfants directs de cette classe.
 */
public abstract class Coordinates implements Movable {
	
	public static int DPD = 50;
	
	private double x;
	private double y;
	private double z;
	
	public Coordinates(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Retourne la coordonnée x de l'ensemble de coordonnées
	 * @return l'abscisse des coordonnées
	 */
	public double getX() {
		return this.x;
	}
	
	/**
	 * Saisie la coordonnée de x de l'ensemble de coordonnées
	 * @param x le nouvel abscisse des coordonnées
	 */
	public void setX(double x) {
		this.x = x;
	}
	
	/**
	 * Saisie la coordonnée de y de l'ensemble de coordonnées
	 * @return l'ordonnée des coordonnées
	 */
	public double getY() {
		return this.y;
	}
	
	/**
	 * Saisie la coordonnée y de l'ensemble de coordonnées
	 * @param y la nouvelle ordonnée des coordonnées
	 */
	public void setY(double y) {
		this.y = y;
	}
	
	/**
	 * Retourne la coordonnée z de l'ensemble de coordonnées
	 * @return la profondeur des coordonnées
	 */
	public double getZ() {
		return this.z;
	}
	
	/**
	 * Saisie la coordonnée z de l'ensemble de coordonnées
	 * @param z la nouvelle profondeur des coordonnées
	 */
	public void setZ(double z) {
		this.z = z;
	}
	
	/**
	 * Mesure la distance entre deux ensembles de coordonnées
	 * @param c les coordonnées comparées aux coordonnées courantes
	 * @return la distance entre les deux ensembles de coordonnées
	 */
	public double distance(Coordinates c) {
		double x = c.getX()-this.x;
		double y = c.getY()-this.y;
		double z = c.getZ()-this.z;
		return Math.sqrt(x*x+y*y+z*z);
	}

	@Override
	public void move(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public void move(double x, double y, double z) {
		this.move(x,y);
		this.z = z;
	}
	
	@Override
	public void move(Coordinates c) {
		this.move(c.getX(), c.getY(), c.getZ());
	}

	@Override
	public void translate(Vector3D v) {
		this.x += v.getX();
		this.y += v.getY();
		this.z += v.getZ();
	}

	@Override
	public void zoom(double value) {
		this.x *= value;
		this.y *= value;
		this.z *= value;
	}

	@Override
	public void rotate(double ax, double ay, double az) {
		this.rotateX(ax);
		this.rotateY(ay);
		this.rotateZ(az);
	}
	
	/**
	 * Effectue une rotation du point par rapport à la coordonnée x
	 * @param angle l'angle de rotation du point
	 */
	private void rotateX(double angle) {
		double y = this.y;
		double z = this.z;
		angle /= DPD;
		this.y = (y * Math.cos(angle) - z * Math.sin(angle));
		this.z = (y * Math.sin(angle) + z * Math.cos(angle));
	}
	
	/**
	 * Effectue une rotation du point par rapport à la coordonnée y
	 * @param angle l'angle de rotation du point
	 */
	private void rotateY(double angle) {
		double x = this.x;
		double z = this.z;
		angle /= DPD;
		this.x = (x * Math.cos(angle) + z * Math.sin(angle));
		this.z = (-x * Math.sin(angle) + z * Math.cos(angle));
	}
	
	/**
	 * Effectue une rotation du point par rapport à la coordonnée z
	 * @param angle l'angle de rotation du point
	 */
	private void rotateZ(double angle) {
		double x = this.x;
		double y = this.y;
		angle /= DPD;
		this.x = (x * Math.cos(angle) - y * Math.sin(angle));
		this.y = (x * Math.sin(angle) + y * Math.cos(angle));
	}
	
	/**
	 * Retourne vrai si deux points sont identiques
	 * @param p le point à comparer avec le point courant
	 * @return vrai si les deux points sont identiques
	 */
	private boolean equals(Coordinates c) {
		return x == c.x && y == c.y && z == c.z;
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof Coordinates) && equals((Coordinates)o);
	}
	
	@Override
	public String toString() {
		return String.format("coordonnées(x:%s, y:%s, z:%s)", x,y,z);
	}
	

}
