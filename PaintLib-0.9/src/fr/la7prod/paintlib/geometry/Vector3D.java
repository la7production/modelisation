/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.geometry;

import fr.la7prod.paintlib.model.Face;

/**
 * En mathématiques, un vecteur est un élément d'un espace vectoriel,
 * ce qui permet d'effectuer des opérations d'addition et de multiplication par un scalaire.
 */
public class Vector3D extends Coordinates implements Cloneable {
	
	public Vector3D() {
		super(0,0,0);
	}
	
	public Vector3D(double x, double y, double z) {
		super(x,y,z);
	}
	
	public Vector3D(Coordinates u, Coordinates v) {
		this(v.getX()-u.getX(), v.getY()-u.getY(), v.getZ()-u.getZ());
	}

	/**
	 * Retourne la norme du vecteur
	 * @return la norme sous forme de double du vecteur
	 */
	public double scale() {
		double x = this.getX();
		double y = this.getY();
		double z = this.getZ();
		return Math.sqrt((x*x) + (y*y) + (z*z));
	}
	
	/**
	 * Retourne le vecteur normal unitaire du vecteur
	 * @return le vecteur normal unitaire du vecteur
	 */
	public Vector3D normalize() {
		Vector3D util = this.clone();
		util.zoom(1.0d / scale());
		return util;
	}
	
	/**
	 * Retourne le vecteur inverse du vecteur courant
	 * @return le vecteur inverse (coordonnées = - coordonnées) du vecteur
	 */
	public Vector3D reverse() {
		return new Vector3D(-this.getX(),-this.getY(),-this.getZ());
	}
	
	/**
	 * Retourne l'angle formé par le vecteur et l'axe x
	 * @return la valeur de l'angle formé par le vecteur et l'axe x
	 */
	public double getAngleX() {
		return Math.acos(dotProduct(this, new Vector3D(1,0,0)) / this.scale());
	}
	
	/**
	 * Retourne l'angle formé par le vecteur et l'axe y
	 * @return la valeur de l'angle formé par le vecteur et l'axe y
	 */
	public double getAngleY() {
		return Math.acos(dotProduct(this, new Vector3D(0,1,0)) / this.scale());
	}
	
	/**
	 * Retourne l'angle formé par le vecteur et l'axe z
	 * @return la valeur de l'angle formé par le vecteur et l'axe z
	 */
	public double getAngleZ() {
		return Math.acos(dotProduct(this, new Vector3D(0,0,1)) / this.scale());
	}
	
	/**
	 * Retourne l'angle formé par le vecteur et l'axe horizontal
	 * @return la valeur de l'angle formé par le vecteur et l'axe horizontal
	 */
	public double getHorizontalAngle() {
		if (this.getX() == 0 && this.getZ() == 0 && this.getY() != 0)
			return 0;
		Vector3D nX = new Vector3D( 1,0,0 );
		Vector3D nZ = new Vector3D( 0,0,1 );
		
		double sX = dotProduct(this,nX);
		double sZ = dotProduct(this,nZ);
		
		Vector3D dX = new Vector3D(sX,0,0);
		Vector3D dZ = new Vector3D(0,0,sZ);
		
		Vector3D dXZ = dX.clone();
		dXZ.translate(dZ);
		dXZ.normalize();
		
		double horizAngle = Math.acos(dotProduct(nZ,dXZ));
		if( dX.getX() < 0 )
			return Math.PI*2f - horizAngle;
		return horizAngle;
	}
	
	/**
	 * Retourne l'angle formé par le vecteur et l'axe vertical
	 * @return la valeur de l'angle formé par le vecteur et l'axe vertical
	 */
	public double getVerticalAngle() {
		return getAngleY();
	}
	
	@Override
	public String toString(){
		return "Vecteur de " + super.toString();
	}
	
	@Override
	public Vector3D clone() {
		return new Vector3D(this.getX(),this.getY(),this.getZ());
	}
	
	/**
	 * Retourne le vecteur normal d'une face
	 * @param face la face pour laquelle on cherche son vecteur normal
	 * @return le vecteur normal de la face passée en paramètre
	 */
	public static Vector3D normal(Face face) {
		Vector3D u = face.getSegment(0).getVector();
		Vector3D v = face.getSegment(1).getVector();
		return Vector3D.crossProduct(u, v);
	}
	
	/**
	 * Calcul le produit vectoriel de deux vecteurs
	 * @param u le premier vecteur
	 * @param v le second vecteur
	 * @return le vecteur créé par le produit vectoriel des deux vecteurs passés en paramètres
	 */
	public static Vector3D crossProduct(Vector3D u, Vector3D v) {
		return new Vector3D(
				u.getY()*v.getZ() - u.getZ()*v.getY(),
				u.getZ()*v.getX() - u.getX()*v.getZ(),
				u.getX()*v.getY() - u.getY()*v.getX());
	}
	
	/**
	 * Retourne le produit scalaire de deux vecteurs
	 * @param u le premier vecteur
	 * @param v le second vecteur
	 * @return le produit scalaire des deux vecteurs passés en paramètres
	 */
	public static double dotProduct(Vector3D u, Vector3D v) {
		return u.getX()*v.getX() + u.getY()*v.getY() + u.getZ()*v.getZ();
	}
	
	/**
	 * Retourne le cosinus obtenu grâce à deux vecteurs
	 * @param u le premier vecteur
	 * @param v le second vecteur
	 * @return le cosinus obtenu via les deux vecteurs passés en paramètres
	 */
	public static double cos(Vector3D u, Vector3D v) {
		return Math.abs(Vector3D.dotProduct(u, v) / (u.scale() * v.scale()));
	}

}
