/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import fr.la7prod.paintlib.gts.GtsCaller;
import fr.la7prod.paintlib.model.Model;
import fr.la7prod.paintlib.model.util.AutoRotate;
import fr.la7prod.paintlib.scene.util.SceneTabCloser;

/**
 * Scenes est un ensemble de scènes divisés en onglets
 * qui peuvent être ajoutés et ou supprimés, tous indépendemment des autres.
 * Chaque scène répond à son propre Model et à son propre SceneManager
 */
public class Scenes extends JTabbedPane implements MouseListener {
	
	private static final long serialVersionUID = -2815798337429338667L;
	
	private Scene currentScene;
	
	public Scenes(Color background) {
		JPanel addpanel = new JPanel();
		addpanel.setBackground(background);
		this.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		this.setPreferredSize(new Dimension(700,700));
		this.setFocusable(false);
		this.addTab("+",addpanel);
		this.addMouseListener(this);
	}

	public Scenes() {
		this(Scene.DEFAULT_BACKGROUND);
	}
	
	/**
	 * Ajoute une scène dans l'ensemble des scènes
	 * @param title le titre de la scène (par défaut le nom du modèle)
	 * @param scene la scène à ajouter
	 */
	public void addTab(String title, Scene scene) {
		super.addTab(title, scene);
		this.setSelectedComponent(scene);
		this.setTabComponentAt(this.getTabCount()-1, new SceneTabCloser(this,scene));
	}

	/**
	 * Saisie la scène courante
	 * @param scene la nouvelle cène courante
	 */
	public void setSelectedComponent(Scene scene) {
		if (scene != null)
			super.setSelectedComponent(scene);
	}
	
	@Override
	public void setSelectedIndex(int index) {
		super.setSelectedIndex(index);
		if (index > 0) {
			if (currentScene != null)
				this.setInBackground(currentScene.getModel().getAutoRotate());
			this.currentScene = (Scene)this.getSelectedComponent();
			this.currentScene.requestFocusInWindow();
			this.setInForeground(currentScene.getModel().getAutoRotate());
		}
	}
	
	/**
	 * Stocke un thread de rotation automatique mis en arrière plan.
	 * C'est à dire que le modèle associé à ce thread n'est actuellement pas visible de l'utilisateur
	 * et n'a donc pas besoin d'être actif. On le met en pause également pour gagner en mémoire.
	 * @param a le thread de rotation automatique à mettre en pause et à stocker
	 */
	private void setInBackground(AutoRotate a) {
		if (a.isActived()) {
			a.waiting();
		}
	}
	
	/**
	 * Réactive un thread de rotation automatique qui se serait retrouvé en arrière plan précédemment.
	 * @param a le thread de rotation automatique à réactiver et à déstocker
	 */
	private void setInForeground(AutoRotate a) {
		if (a.isWaiting()) {
			a.start();
		}
	}
	
	/**
	 * Supprime une scène de l'ensemble des scènes
	 * La scène 
	 * @param scene la scène à supprimer
	 */
	@SuppressWarnings("deprecation")
	public void remove(Scene scene) {
		super.remove(scene);
		currentScene.getModel().getAutoRotate().stop();
		if (currentScene == scene) {
			if (this.getTabCount() > 1)
				currentScene = (Scene)this.getSelectedComponent();
			else
				currentScene = null;
		}
	}
	
	/**
	 * Remplace une scène par une autre.
	 * La scène est fermée et une autre est ajoutée
	 * puis replacé à l'indice de la scène fermée.
	 * @param toReplace la scène à remplacer
	 * @param replacer la scène remplaçante
	 */
	private void replace(Scene toReplace, Scene replacer) {
		int index = this.indexOfComponent(toReplace);
		this.remove(toReplace);
		this.insertTab(null, null, replacer, null, index);
		this.setTabComponentAt(index, new SceneTabCloser(this,replacer));
		this.setSelectedIndex(index);
	}
	
	/**
	 * Retourne la scène actuellement observée
	 * @return la scène courante dans l'ensemble des scènes
	 */
	public Scene getCurrentScene() {
		return currentScene;
	}
	
	/**
	 * Ajoute une scène à l'ensemble des scènes.
	 * Si le modèle de la scène est déjà présent, on demande à l'utilisateur
	 * s'il veut écraser le modèle
	 * @param scene la scène à ajouter
	 */
	public void add(Scene scene) {
		int n;
		if ((n = this.indexOfComponent(scene)) == -1) {
			this.addTab(scene.getModel().getName(), scene);
			scene.getLight().getPoint().move(this.getMousePosition().getX(),this.getMousePosition().getY());
		}
		else {
			this.setSelectedIndex(n);
			int result = showOptions("Ouvrir l'autre scène", "Continuer avec la même scène",
					"Modèle déjà existant",
					"Ce modèle est déjà présent dans une scène");
			if (result == JOptionPane.YES_OPTION) {
				this.replace((Scene)this.getComponentAt(n), scene);
			}
		}
	}
	
	/**
	 * Ouvre un modèle et crée une nouvelle scène
	 * à intégrer dans l'ensemble des scènes.
	 */
	private void openModel() {
		try {
			Model model = new GtsCaller().open();
			Scene scene = new Scene(model, new Light());
			scene.addSceneManager(new SceneManager(scene));
			add(scene);
		} catch (NumberFormatException | IOException e) {
			int result = showOptions("Choisir une autre model", "Annuler",
					"Erreur de chargement",
				    "Le fichier semble corrompu ou ne contient aucune model");
			if (result == JOptionPane.YES_OPTION)
				openModel();
			else
				this.setSelectedComponent(currentScene);
		} catch(NullPointerException e) {
			this.setSelectedComponent(currentScene);
		}
	}
	
	/**
	 * Affiche une boite de dialogue à l'utilisateur lorsqu'il doit faire un choix entre deux options
	 * @param yes l'option s'il veut continuer l'action qu'il s'apprête à faire
	 * @param no l'option s'il veut annuler l'action qu'il s'apprête à faire
	 * @param title le titre de la boite de dialogue
	 * @param message le message expliquant l'action que l'utilisateur s'apprête à faire
	 * @return un 0 s'il continue l'action, 1 sinon
	 */
	private int showOptions(String yes, String no, String title, String message) {
		String options[] = { yes, no };
		return JOptionPane.showOptionDialog(this,message,title,
				JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,
				null,options,options[1]);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (this.getSelectedIndex() == 0) {
			this.setSelectedComponent(currentScene);
			this.openModel();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}

}
