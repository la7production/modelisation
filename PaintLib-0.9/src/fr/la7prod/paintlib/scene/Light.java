/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene;

import java.awt.Color;

import fr.la7prod.paintlib.geometry.Vector3D;
import fr.la7prod.paintlib.model.Barycentre;
import fr.la7prod.paintlib.model.Face;
import fr.la7prod.paintlib.model.Point3D;
import fr.la7prod.paintlib.model.util.Updatable;

/**
 * Light est une source lumineuse qui peut être ajoutée à une scène.
 */
public class Light extends Updatable {
	
	public static final Color DEFAULT_COLOR = new Color(165, 38, 10);
	private Point3D point;
	private Color color;
	private double brightness;
	private boolean on;
	private boolean movable;
	
	public Light() {
		this(DEFAULT_COLOR);
	}
	
	public Light(Color color) {
		this(new Point3D(0,0,100), color);
	}
	
	public Light(Point3D point, Color color) {
		this.point = point;
		this.color = color;
		this.brightness = 0.00008;
		this.on = true;
		this.movable = true;
	}
	
	/**
	 * Retourne vrai si la source lumineuse est allumée
	 * @return vrai si la source lumineuse est allumée
	 */
	public boolean isOn() {
		return this.on;
	}
	
	/**
	 * Retourne vrai si la source lumineuse peut être déplacée
	 * @return vrai si la source lumineuse peut être déplacée
	 */
	public boolean isMovable() {
		return this.movable;
	}
	
	/**
	 * Agit comme un interrupteur c'est à dire que cette méthode permet d'allumer ou d'éteindre
	 * la source lumineuse selon son état précédent
	 */
	public void toggleOnOff() {
		this.on = !this.on;
	}
	
	/**
	 * Permet de déplacer la source lumineuse ou au contraire de la fixer à un point précis
	 */
	public void toggleMove() {
		this.movable = !this.movable;
	}
	
	/**
	 * Augmente la luminosité de la source lumineuse
	 */
	public void brighter() {
		this.brightness /= 2;
	}
	
	/**
	 * Diminue la luminosité de la source lumineuse
	 */
	public void darker() {
		this.brightness *= 2;
	}

	/**
	 * Retourne le point correspondant à la source lumineuse
	 * @return le point correspondant à la position du SpotLight
	 */
	public Point3D getPoint() {
		return point;
	}

	/**
	 * Retourne la couleur diffusée par la source lumineuse
	 * @return la couleur diffusée par le SpotLight
	 */
	public Color getColor() {
		return color;
	}
	
	/**
	 * Modifie la couleur diffusée par la source lumineuse
	 * @param color la nouvelle couleur du SpotLight
	 */
	public void setColor(Color color){
		this.color = color;
	}
	
	/**
	 * Retourne la couleur de la face selon le pourcentage de lumière sur elle
	 * On calcule donc un pourcentage qui correspond à la luminosité de la face selon une source lumineuse.
	 * 1- Pour cela, on récupère un vecteur normal de la face.
	 * 2- On récupère ensuite le vecteur qui va de la source lumineuse au barycentre de la face.
	 * 3- On calcule le produit sclaire du vecteur normal et du vecteur de la source.
	 * 4- Enfin on retourne le calcul du pourcentage de luminosité voulue.
	 * @param face la face qui subit le changement de lumière
	 */
	public void setColorOf(Face face) {
		//new PhongLight(this).setColorOf(face);
		if (!on) {
			face.setColor(Color.BLACK);
		}
		else {
			Barycentre bary = face.getBarycentre();
			Vector3D nf = Vector3D.normal(face);
			Vector3D nl = new Vector3D(bary, point);
			double cos = Vector3D.cos(nf,nl);
			double dist = nl.scale();
			dist *= dist * brightness;
			
			face.setColor(new Color(
					(int)(Math.min(255,color.getRed() * cos / dist)),
					(int)(Math.min(255,color.getGreen() * cos / dist)),
					(int)(Math.min(255, color.getBlue() * cos / dist))));
		}
	}
	
	@Override
	public String toString() {
		return "Light[Color=" + color + ", Point=" + point + "]";
	}

}
