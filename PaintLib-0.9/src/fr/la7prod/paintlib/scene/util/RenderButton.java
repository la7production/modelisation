/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import fr.la7prod.paintlib.scene.Scene;
import fr.la7prod.paintlib.scene.Scene.Render;

/**
 * RenderButton est un bouton associé à une scène afin de modifier le rendu d'un modèle
 * dans cette scène. Les différents rendus sont définis par l'enumeration Render.
 * @see fr.la7prod.paintlib.scene.Scene.Render
 */
public class RenderButton extends SceneButton {

	private static final long serialVersionUID = 8693205342879747585L;
	
	public RenderButton(final Scene scene, final Render render, String tooltip) {
		super();
		this.setIcon(render.name().toLowerCase());
		this.setToolTipText(tooltip);
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				scene.setRender(render);
				scene.getModel().update();
			}
			
		});
	}

	@Override
	public void update(Observable obs, Object o) {
		// Nothing TODO
		
	}

}
