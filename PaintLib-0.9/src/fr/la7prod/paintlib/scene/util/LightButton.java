/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import fr.la7prod.paintlib.scene.Light;

/**
 * LightButton est un bouton associé à une source lumineuse permettant
 * de l'allumer ou de l'éteindre selon son état précédent.
 */
public class LightButton extends SceneButton {

	private static final long serialVersionUID = -7383091257072987715L;

	public LightButton(final Light light) {
		super();
		light.addObserver(this);
		this.setOnIcon();
		this.setToolTipText("Allume/Eteint la lumière");
		this.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				light.toggleOnOff();
				light.update();
			}
			
		});
	}
	
	/**
	 * Met l'icone de la source lumineuse allumée
	 */
	private void setOnIcon() {
		this.setIcon("lightOn");
	}
	
	/**
	 * Met l'icone de la source lumineuse éteinte
	 */
	private void setOffIcon() {
		this.setIcon("lightOff");
	}

	@Override
	public void update(Observable obs, Object o) {
		if (((Light)o).isOn()) {
			setOnIcon();
		}
		else {
			setOffIcon();
		}
	}

}
