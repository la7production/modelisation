/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene.util;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import fr.la7prod.commons.file.AppDataConfiguration;
import fr.la7prod.paintlib.scene.Scene;

/**
 * CaptureButton est un bouton qui permet de prendre une photo du modèle
 * que l'utilisateur est en train d'utiliser.
 */
public class CaptureButton extends SceneButton {

	private static final long serialVersionUID = -3921645257393182757L;
	
	public CaptureButton(final Scene scene, final JPanel buttons) {
		super();
		this.setIcon("capture");
		this.setToolTipText("Prendre une photo du modèle");
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					buttons.setVisible(false);
					String name = scene.getModel().getName();
					SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd_HH-mm-ss");
					AppDataConfiguration a = new AppDataConfiguration(".sam");
					a.load();
					BufferedImage image = new Robot().createScreenCapture(
							new Rectangle(
									(int)scene.getLocation().getX(),
									(int)scene.getLocation().getY(),
									scene.getWidth(),
									scene.getHeight()-buttons.getHeight()));
					scene.paint(image.getGraphics());
					buttons.setVisible(true);
					ImageIO.write(image, "png",
							new File(a.getProgramPath() + File.separator + "screenshots" + File.separator
									+ name.substring(0, name.indexOf(".gts"))
									+ "_" + sdf.format(new Date())
									+ ".png"));
				} catch (AWTException ex) {
					ex.printStackTrace();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
				buttons.setVisible(true);
			}
			
		});
	}

	@Override
	public void update(Observable paramObservable, Object paramObject) {
		// Nothing TODO
		
	}

}
