/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene.util;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * SceneButton est un mini bouton clipsable dans une scène.
 * Ce bouton pourra apporter des modifications dans la scène vis à vis du modèle
 * ou de la source lumineuse.
 */
public abstract class SceneButton extends JButton implements Observer {

	private static final long serialVersionUID = 1L;
	
	public SceneButton() {
		this.setPreferredSize(new Dimension(32,32));
		this.setContentAreaFilled(false);
		this.setFocusable(false);
		this.setFocusPainted(false);
		this.setOpaque(false);
		this.setCursor(new Cursor(Cursor.HAND_CURSOR));
	}
	
	public SceneButton(Icon icon) {
		this();
		this.setIcon(icon);
	}
	
	/**
	 * Initialise un bouton de rendu présent dans la scène
	 * @param resourceName le lien de l'image à mettre sur le bouton
	 */
	public void setIcon(String resourceName) {
		URL url = this.getClass().getResource("/images/" + resourceName + ".png");
		BufferedImage img = null;
		try {
			img = ImageIO.read(url);
			this.setIcon(new ImageIcon(img));
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}

}
