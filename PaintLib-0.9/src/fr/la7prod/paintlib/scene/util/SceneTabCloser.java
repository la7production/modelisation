/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene.util;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.la7prod.paintlib.scene.Scene;
import fr.la7prod.paintlib.scene.Scenes;

/**
 * Le SceneTabCloser est un système de label + bouton de fermeture
 * qui est couplé aux onglets d'un JTabbedPane classique afin de permettre
 * à l'utilisateur de fermer facilement un onglet du JTabbedPane.
 */
public class SceneTabCloser extends JPanel implements ActionListener, MouseListener {

	private static final long serialVersionUID = 8914887973851419510L;

	private Scenes scenes;
	private Scene scene;
	private JButton close;
	
	public SceneTabCloser(Scenes scenes, Scene scene) {
		JLabel name = new JLabel(scene.getModel().getName());
		this.scenes = scenes;
		this.scene = scene;
		this.close = new JButton("x");
		this.setOpaque(false);
		
		close.setBorderPainted(false);
		close.setContentAreaFilled(false);
		close.setFocusable(false);
		close.setCursor(new Cursor(Cursor.HAND_CURSOR));
		close.setFont(new Font("Arial",Font.CENTER_BASELINE,13));
		close.setMargin(new Insets(0,0,0,0));
		close.addActionListener(this);
		close.addMouseListener(this);
		name.setFont(new Font("Arial",Font.CENTER_BASELINE,10));
		this.add(name);
		this.add(close);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		close.setForeground(Color.RED);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		close.setForeground(Color.DARK_GRAY);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		scenes.remove(scene);
	}

}
