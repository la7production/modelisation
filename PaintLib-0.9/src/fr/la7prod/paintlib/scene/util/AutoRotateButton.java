/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import fr.la7prod.paintlib.model.Model;

/**
 * AutoRotateButton est un bouton associé à un thread de rotation automatique d'un modèle
 * contenu dans une scène. Il permet d'activer ou non la rotation automatique.
 */
public class AutoRotateButton extends SceneButton {

	private static final long serialVersionUID = -5382092956884709535L;
	
	public AutoRotateButton(final Model model) {
		super();
		model.addObserver(this);
		this.setOnIcon();
		this.setToolTipText("Active/Désactive la rotation automatique du modèle");
		this.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				model.toggleRotate();
				model.update();
			}
			
		});
	}
	
	/**
	 * Met l'icone de la rotation automatique démarrée
	 */
	private void setOnIcon() {
		this.setIcon("rotateOn");
	}
	
	/**
	 * Met l'icone de la rotation automatique arrêtée
	 */
	private void setOffIcon() {
		this.setIcon("rotateOff");
	}

	@Override
	public void update(Observable obs, Object o) {
		if (!((Model)o).getAutoRotate().isActived()) {
			setOffIcon();
		}
		else {
			setOnIcon();
		}
	}

}
