/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import fr.la7prod.paintlib.model.Model;
import fr.la7prod.paintlib.model.Point3D;
import fr.la7prod.paintlib.scene.util.AutoRotateButton;
import fr.la7prod.paintlib.scene.util.CaptureButton;
import fr.la7prod.paintlib.scene.util.LightButton;
import fr.la7prod.paintlib.scene.util.RenderButton;
import fr.la7prod.paintlib.scene.util.SceneButton;

/**
 * Une scène permet comme son nom l'indique, d'observer un model.
 * Elle est également composée d'une source lumineuse et il est possible
 * de lui attribuer un fond coloré. On peut aussi changer la façon de dessiner le model.
 */
public class Scene extends JPanel implements Observer {
	
	/**
	 * Render est une énumération permettant de savoir comment un objet modelisable
	 * doit être dessiné (rendu), c'est à dire s'il doit être remplit, si on doit dessiner ses contours,
	 * ou si on ne doit dessiner que ses sommets et son barycentre.
	 */
	public enum Render {
		FILLED, OUTLINED, POINTED;
	}

	public static final Color DEFAULT_BACKGROUND = Color.DARK_GRAY;
	private static final long serialVersionUID = -8871512460715807565L;
	
	private Light light;
	private Model model;
	private Render render;
	private SceneButton[] sceneButtons;
	private boolean ready;
	
	public Scene(Model model, Light light) {
		this(model,light,DEFAULT_BACKGROUND);
	}
	
	public Scene(Model model, Light light, Color background) {
		JPanel buttons = new JPanel(new FlowLayout(FlowLayout.TRAILING));
		this.setModel(model);
		this.light = light;
		this.light.addObserver(this);
		this.render = Render.FILLED;
		this.sceneButtons = new SceneButton[6];
		this.sceneButtons[0] = new CaptureButton(this, buttons);
		this.sceneButtons[1] = new LightButton(light);
		this.sceneButtons[2] = new AutoRotateButton(model);
		this.sceneButtons[3] = new RenderButton(this,Render.FILLED,"Dessine pleinement le modèle");
		this.sceneButtons[4] = new RenderButton(this,Render.OUTLINED,"Dessine les segments du modèle");
		this.sceneButtons[5] = new RenderButton(this,Render.POINTED,"Dessine les points du modèle");
		this.setBackground(background);
		this.setLayout(new BorderLayout());
		
		buttons.setOpaque(false);
		for(SceneButton b : sceneButtons)
			buttons.add(b);
		this.add(buttons, BorderLayout.SOUTH);
	}
	
	/**
	 * Associe un SceneManager à la scène
	 * @param sm le SceneManager a associer à la scène
	 */
	public void addSceneManager(SceneManager sm) {
		this.addKeyListener(sm);
		this.addMouseListener(sm);
		this.addMouseWheelListener(sm);
		this.addMouseMotionListener(sm);
	}
	
	/**
	 * Dissocie la scène d'un SceneManager
	 * @param sm le SceneManager a dissocier de la scène
	 */
	public void removeSceneManager(SceneManager sm) {
		if (sm.getAssociatedScene() == this) {
			this.removeKeyListener(sm);
			this.removeMouseListener(sm);
			this.removeMouseWheelListener(sm);
			this.removeMouseMotionListener(sm);
		}
	}
	
	/**
	 * Retourne le bouton qui permet de prendre une photo du modèle
	 * @return le bouton pour prendre en photo le modèle
	 */
	public SceneButton getCaptureButton() {
		return this.sceneButtons[0];
	}
	
	/**
	 * Retourne le bouton qui permet de manipuler la lumière avec la souris
	 * @return le bouton pour manipuler la lumière avec la souris
	 */
	public SceneButton getLightButton() {
		return this.sceneButtons[1];
	}
	
	/**
	 * Retourne le bouton qui permet d'activer ou non la rotation automatique du modèle
	 * @return le bouton pour manipuler la rotation automatique du modèle
	 */
	public SceneButton getAutoRotateButton() {
		return this.sceneButtons[2];
	}
	
	/**
	 * Retourne le bouton qui permet de faire l'affichage plein du model
	 * @return le bouton pour l'affichage plein du model
	 */
	public SceneButton getFilledButton() {
		return this.sceneButtons[3];
	}
	
	/**
	 * Retourne le bouton qui permet de faire l'affichage en segments du model
	 * @return le bouton pour l'affichage en segments du model
	 */
	public SceneButton getOutlinedButton() {
		return this.sceneButtons[4];
	}
	
	/**
	 * Retourne le bouton qui permet de faire l'affichage en points du model
	 * @return le bouton pour l'affichage en points du model
	 */
	public SceneButton getPointedButton() {
		return this.sceneButtons[5];
	}
	
	/**
	 * Retourne le model de la scène
	 * @return le model de la scène
	 */
	public Model getModel() {
		return this.model;
	}
	
	/**
	 * Retourne la source lumineuse de la scène
	 * @return la source lumineuse de la scène
	 */
	public Light getLight() {
		return this.light;
	}
	
	/**
	 * Retourne le type de rendu pour le model
	 * @return le type de rendu pour le model
	 */
	public Render getRender() {
		return this.render;
	}
	
	/**
	 * Saisie le model qui sera affichée dans la scène.
	 * Lors de son premier affichage, le model est située au centre de la scène.
	 * @param model le model qui sera visible pour l'utilisateur
	 */
	public void setModel(Model model) {
		this.ready = false;
		if (this.model != null && this.model.countObservers() > 0)
				this.model.deleteObserver(this);
		this.model = model;
		this.model.addObserver(this);
	}
	
	/**
	 * Saisie le rendu du model à l'écran dans la scène
	 * @param render le rendu du model à l'écran dans la scène
	 */
	public void setRender(Render render) {
		this.render = render;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (ready)
			model.draw(g, light, render);
		else
			prepareModel();
	}
	
	/**
	 * Prépare le modèle pour l'affichage
	 */
	
	private void prepareModel() {
		Point3D[] vertices = model.getVertices();
		Point3D p;
		Point3D pxmin = vertices[0];
		Point3D pxmax = pxmin;
		Point3D pymin = pxmin;
		Point3D pymax = pxmin;
		double coeff;
		for(int i=1; i<vertices.length; i++) {
			p = vertices[i];
			if (p.getX() < pxmin.getX())
				pxmin = p;
			else if (p.getX() > pxmax.getX())
				pxmax = p;
			if (p.getY() < pymin.getY())
				pymin = p;
			else if (p.getY() > pymax.getY())
				pymax = p;
		}
		coeff = this.getWidth()/(pxmax.getX()-pxmin.getX());
		if (pxmax.getX()-pxmin.getX() < pymax.getY()-pymin.getY())
			coeff = this.getHeight()/(pymax.getY()-pymin.getY());
		model.move(this.getWidth()/2, this.getHeight()/2);
		model.zoom(coeff);
		while(pxmax.getX() > this.getWidth() || pxmin.getX() < 0
				|| pymin.getY() < 0 || pymax.getY() > this.getHeight()) {
			model.zoom(0.9);
		}
		ready = true;
		model.update();
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof Scene) && model.equals(((Scene)o).model);
	}

	@Override
	public void update(Observable obs, Object o) {
		this.repaint();
	}

}
