/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import fr.la7prod.paintlib.geometry.Vector3D;
import fr.la7prod.paintlib.model.Point3D;
import fr.la7prod.paintlib.scene.Scene.Render;

/**
 * SceneManager est le contrôleur permettant d'associer une scene et un objet modelisable.
 * C'est le SceneManager qui indique à la scene si l'objet modelisable subit des changements d'état.
 * La scene s'adapte en conséquence et effectue un repaint par exemple.
 */
public class SceneManager implements SceneController, KeyListener, MouseListener, MouseWheelListener, MouseMotionListener {
	
	private Scene scene;
	private int mouse;
	private Point3D mousepos;
	
	public SceneManager(Scene scene) {
		this.scene = scene;
		this.mousepos = new Point3D();
	}
	
	/**
	 * Retourne la scène associée au SceneManager
	 * @return la scène associée au SceneManager
	 */
	public Scene getAssociatedScene() {
		return this.scene;
	}
	
	@Override
	public void changeAutoRotateState() {
		scene.getModel().toggleRotate();
		scene.getModel().update();
	}
	
	@Override
	public void changeLightState() {
		scene.getLight().toggleOnOff();
		scene.getLight().update();
	}

	@Override
	public void changeRender(Render render) {
		this.scene.setRender(render);
	}
	
	@Override
	public void changeBrightness(int in) {
		if (in > -1)
			scene.getLight().brighter();
		else
			scene.getLight().darker();
		scene.getLight().update();
	}
	
	@Override
	public void executeZoom(double value) {
		scene.getModel().zoom(value);
		scene.getLight().getPoint().setZ(scene.getLight().getPoint().getZ()*value);
		scene.getModel().update();
	}
	
	@Override
	public void executeTranslation(Vector3D v) {
		Vector3D res = new Vector3D(mousepos, v);
		scene.getModel().translate(new Vector3D(res.getX(),res.getY(),0));
		mousepos.move(v);
		scene.getModel().update();
	}
	
	@Override
	public void executeRotation(double ax, double ay, double az) {
		double x = mousepos.getX() - ax;
		double y = mousepos.getY() - ay;
		final double DPD = Math.PI/10;
		double xdpd = x*DPD;
		double ydpd = y*DPD;
		scene.getModel().rotate(ydpd,-xdpd,0);
		scene.getModel().getAutoRotate().move(ydpd,-xdpd,0);
		mousepos.move(ax,ay,az);
		scene.getModel().update();
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		char key = e.getKeyChar();
		if (key == 'l') {
			this.scene.getLight().toggleMove();
		}
		else if (key == '+')
			this.changeBrightness(1);
		else if (key == '-')
			this.changeBrightness(-1);
		else if (key == 'f') {
			this.changeLightState();
		}
		else if (key == 'r') {
			this.changeAutoRotateState();
		}
		else {
			if (e.getKeyCode() == KeyEvent.VK_F12) {
				this.scene.getCaptureButton().doClick();
			}
			else if (key == 'i') {
				this.changeRender(Render.FILLED);
			}
			else if (key == 'o') {
				this.changeRender(Render.OUTLINED);
			}
			else if (key == 'p') {
				this.changeRender(Render.POINTED);
			}
		}
		if (scene.getLight().isOn() && scene.getLight().isMovable())
			scene.getLight().update();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		this.mouse = e.getButton();	
		this.mousepos.move(e.getX(),e.getY(),0);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		this.mouse = -1;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (mouse == MouseEvent.BUTTON1) {
			this.executeTranslation(new Vector3D(e.getX(),e.getY(),0));
		}
		else if (mouse == MouseEvent.BUTTON3) {
			this.executeRotation(e.getX(),e.getY(),0);
		}
		if (scene.getLight().isOn() && scene.getLight().isMovable())
			scene.getLight().getPoint().move(e.getX(),e.getY());
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (scene.getLight().isOn() && scene.getLight().isMovable()) {
			scene.getLight().getPoint().move(e.getX(),e.getY());
			scene.getLight().update();
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		this.executeZoom(Math.abs(-e.getPreciseWheelRotation()+0.1));
	}

}
