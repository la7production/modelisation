/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene;

import fr.la7prod.paintlib.geometry.Vector3D;
import fr.la7prod.paintlib.scene.Scene.Render;

/**
 * Un SceneController est une interface regroupant des méthodes d'exécutions.
 * Il est implémenté par le SceneManager pour manipuler le modèle et la source lumineuse d'une scène
 * sans avoir besoin explicitement de l'accès au modèle, à la source lumineuse et à la scène.
 * 
 * Le SceneControleur est également utile pour généraliser la modification des composants d'une scène,
 * que l'on utilise des Listener (comme l'utilisation de la souris et/ou du clavier) ou
 * que l'on utilise des composants SWING (JSlider, JSpinner...).
 * 
 * Note: les composants SWING requiert de traduire leur utilisation sous forme de Vector3D
 * Exemple: 3 JSliders pour une translation modifient en réalité les valeurs x,y,z d'un Vector3D
 */
public interface SceneController {
	
	/**
	 * Modifie l'état activé/désactivé de la rotation automatique du modèle dans la scène
	 */
	public void changeAutoRotateState();
	
	/**
	 * Modifie l'état allumé/éteint de la source lumineuse de la scène
	 */
	public void changeLightState();
	
	/**
	 * Modifie le rendu du modèle dans la scène
	 * @param render le nouveau rendu du modèle dans la scène
	 */
	public void changeRender(Render render);
	
	/**
	 * Modifie l'intensité de la source lumineuse dans la scène.
	 * Si la valeur passée en paramètre est positive, on augmente l'intensité
	 * sinon on la réduit.
	 * @param in la valeur qui indique si l'intensité doit être augmentée ou réduite
	 */
	public void changeBrightness(int in);
	
	/**
	 * Effectue un zoom des composants d'une (modèle + source lumineuse)
	 * @param value la valeur d'agrandissement
	 */
	public void executeZoom(double value);
	
	/**
	 * Effectue une translation sur un modèle d'une scène
	 * @param v le vecteur de translation
	 */
	public void executeTranslation(Vector3D v);
	
	/**
	 * Effectue une rotation sur un modèle d'une scène
	 * @param ax l'angle par rapport à l'axe x
	 * @param ay l'angle par rapport à l'axe y
	 * @param az l'angle par rapport à l'axe z
	 */
	public void executeRotation(double ax, double ay, double az);

}
