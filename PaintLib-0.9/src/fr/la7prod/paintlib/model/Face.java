/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import fr.la7prod.paintlib.model.util.Modelisable;
import fr.la7prod.paintlib.scene.Light;
import fr.la7prod.paintlib.scene.Scene.Render;

/**
 * Une face est un triangle composé de 3 points communs à 3 segments
 * considéré comme un objet Modelisable
 * @see fr.la7prod.paintlib.model.util.Modelisable
 */
public class Face extends Modelisable {

	public static final int NBPOINTS = 3;
	private Polygon triangle;
	private Color color;
	
	public Face(Segment s1, Segment s2, Segment s3) {
		super("Face");
		this.triangle = new Polygon();
		this.triangle.npoints = NBPOINTS;
		this.triangle.xpoints = new int[NBPOINTS];
		this.triangle.ypoints = new int[NBPOINTS];
		this.setSegment(new Segment[]{s1,s2,s3});
		this.setVertices(commonVertices());
		this.getBarycentre().calculate();
	}
	
	/**
	 * Trouve les points communs des 3 segments composant la face
	 * On considère de ce fait que les 3 segments ont forcément
	 * un point en commun avec l'un ou l'autre des 2 autres segments
	 * Ces points sont en réalité les sommets de la face
	 * @return le tableau contenant les sommets de la face
	 */
	private Point3D[] commonVertices() {
		Segment s1 = this.getSegment(0);
		Segment s2 = this.getSegment(1);
		Point3D[] points = new Point3D[NBPOINTS];
		points[0] = s1.getA();
		points[1] = s1.getB();
		if (s2.getA().equals(points[0]) || s2.getA().equals(points[1]))
			points[2] = s2.getB();
		else
			points[2] = s2.getA();
		return points;
	}
	
	/**
	 * Retourne la couleur de la face
	 * @return la couleur de la face
	 */
	public Color getColor() {
		return this.color;
	}
	
	/**
	 * Saisie une nouvelle couleur pour la face
	 * @param color la nouvelle couleur de la face
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	/**
	 * Indique si un point est contenu dans la face
	 * @param p le point contenu dans la face ou non
	 * @return vrai si le point est contenu dans la face
	 */
	public boolean contains(Point3D p) {
		return triangle.contains(p.getX(), p.getY());
	}
	
	@Override
	public synchronized void draw(Graphics g, Light spot, Render render) {
		for(int i=0; i<NBPOINTS; i++) {
			triangle.xpoints[i] = (int)this.getVertex(i).getX();
			triangle.ypoints[i] = (int)this.getVertex(i).getY();
		}
		spot.setColorOf(this);
		g.setColor(color);
		if (render.equals(Render.FILLED))
			g.fillPolygon(triangle);
		else if (render.equals(Render.OUTLINED))
			g.drawPolygon(triangle);
		else if (render.equals(Render.POINTED)) {
			int x = (int)getBarycentre().getX();
			int y = (int)getBarycentre().getY();
			for(int i=0; i<NBPOINTS; i++) {
				g.drawLine(triangle.xpoints[i], triangle.ypoints[i], triangle.xpoints[i], triangle.ypoints[i]);
			}
			g.drawLine(x,y,x,y);
		}
	}
	
	@Override
	public String toString() {
		Point3D[] vertex = this.getVertices();
		return String.format("Face:\n%s\n%s\n%s",vertex[0],vertex[1], vertex[2]);
	}

}
