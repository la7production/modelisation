/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.model;

import fr.la7prod.paintlib.geometry.Vector3D;
import fr.la7prod.paintlib.model.util.Identifiable;

/**
 * Un segment est caractérisé par deux points et un identifiant
 * qui sera utilisé par ses faces associées dans le fichier .gts
 */
public class Segment implements Identifiable {
	
	public static int NBINSTANCES = 0;
	public static final int NBPOINTS = 2;
	
	private int id;
	private Point3D p1;
	private Point3D p2;
	
	public Segment(Point3D p1, Point3D p2) {
		this.id = ++NBINSTANCES;
		this.p1 = p1;
		this.p2 = p2;
	}
	
	@Override
	public int getNumero() {
		return this.id;
	}
	
	/**
	 * Retourne le premier point du segment noté A
	 * @return le premier point du segment
	 */
	public Point3D getA() {
		return this.p1;
	}
	
	/**
	 * Retourne le second point du segment noté B
	 * @return le second point du segment
	 */
	public Point3D getB() {
		return this.p2;
	}
	
	/**
	 * Retourne le vecteur obtenu à partir du segment
	 * @return le vecteur obtenu grâce à la soustraction des deux points du segment
	 */
	public Vector3D getVector() {
		return new Vector3D(
				p2.getX()-p1.getX(),
				p2.getY()-p1.getY(),
				p2.getZ()-p1.getZ());
	}
	
	@Override
	public String toString() {
		return String.format("Segment:\n%s\n%s", p1, p2);
	}

}
