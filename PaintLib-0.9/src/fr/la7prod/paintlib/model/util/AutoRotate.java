/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.model.util;

import fr.la7prod.paintlib.model.Model;

/**
 * AutoRotate permet d'effectuer une rotation
 * en continue du model voulu
 */
public class AutoRotate extends Thread {
	
	public enum State {
		ACTIVED, PAUSED, WAITING;
	}
	
	public static int SLEEP = 30;
	
	private Model model;
	private double ax;
	private double ay;
	private double az;
	private State state;
	
	public AutoRotate(Model model) {
		this.model = model;
		this.ax = Math.PI/2;
		this.ay = Math.PI/2;
		this.az = 0;
		this.state = State.PAUSED;
		super.start();
	}
	
	/**
	 * Modifie les angles de la rotation automatique
	 * @param ax l'angle par rapport à l'axe x
	 * @param ay l'angle par rapport à l'axe y
	 * @param az l'angle par rapport à l'axe z
	 */
	public void move(double ax, double ay, double az) {
		this.ax = ax;
		this.ay = ay;
		this.az = az;
	}
	
	/**
	 * Agit comme un interrupteur c'est à dire qu'il active ou désactive la rotation automatique
	 * selon son état précédent
	 */
	public synchronized void toggleOnOff() {
		if (this.isActived())
			this.pause();
		else
			this.start();
	}
	
	/**
	 * Indique si la rotation automatique est activée
	 * @return vrai si la rotation automatique est à l'état ACTIVED
	 */
	public synchronized boolean isActived() {
		return this.state.equals(State.ACTIVED);
	}
	
	/**
	 * Indique si la rotation automatique est en pause
	 * @return vrai si la rotation automatique est à l'état PAUSED
	 */
	public synchronized boolean isPaused() {
		return this.state.equals(State.PAUSED);
	}
	
	/**
	 * Indique si la rotation automatique est en attente en arrière plan.
	 * Etre mis en arrière plan signifie que le modèle associé à la rotation automatique
	 * n'est actuellement pas observé par l'utilisateur. La rotation automatique peut
	 * donc être mis en pause en attendant que le modèle revienne au premier plan.
	 * @return vrai si la rotation automatique est à l'état WAITING
	 */
	public synchronized boolean isWaiting() {
		return this.state.equals(State.WAITING);
	}
	
	/**
	 * Active la rotation automatique et en informe
	 * les observers susceptibles d'avoir un regard sur le thread.
	 */
	public synchronized void start() {
		this.state = State.ACTIVED;
		this.sleep();
		this.notifyAll();
	}
	
	/**
	 * Met la rotation automatique en pause
	 */
	public synchronized void pause() {
		this.state = State.PAUSED;
	}
	
	/**
	 * Met la rotation automatique en arrière plan en attendant d'être réutilisée.
	 * Etre mis en arrière plan signifie que le modèle associé à la rotation automatique
	 * n'est actuellement pas observé par l'utilisateur. La rotation automatique peut
	 * donc être mis en pause en attendant que le modèle revienne au premier plan.
	 */
	public synchronized void waiting() {
		this.state = State.WAITING;
	}
	
	/**
	 * Met le thread en attente si la rotation automatique est en pause
	 * ou bien le met à l'état sleep pendant quelques secondes.
	 */
	private synchronized void sleep() {
		if (!this.isActived()) {
			try {
				wait();
			} catch(InterruptedException e) {}
		}
		else {
			try {
				Thread.sleep(SLEEP);
			} catch(InterruptedException e) {}
		}
	}
	
	@Override
	public void run(){
		while(true){
			this.sleep();
			model.rotate(ax, ay, az);
			model.update();
		}
	}
}