/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.model.util;

import java.util.Observable;

/**
 * Un objet dit Updatable (qui peut être mis à jour) est un objet Observable
 * possédant une méthode update() qui permet d'indiquer à ses observeurs
 * lorsqu'il subit un changement d'état ou des modifications.
 */
public abstract class Updatable extends Observable {
	
	/**
	 * Indique que l'objet updatable a subit une modification, un changement d'état
	 * et permet de prévenir les observeurs de l'objet des changements effectués.
	 * Cela aura pour conséquence d'appeler la méthode update() de tous les observeurs de la vue.
	 * Note: Les observeurs étendent la classe Observer de java.
	 * @param o l'objet a modifier en conséquence par les observeurs
	 */
	public void update(Object o) {
		this.setChanged();
		this.notifyObservers(o);
	}
	
	/**
	 * Indique que l'objet updatable a subit une modification, un changement d'état
	 * et permet de prévenir les observeurs de l'objet des changements effectués.
	 * Cela aura pour conséquence d'appeler la méthode update() de tous les observeurs de la vue.
	 * Note: Les observeurs étendent la classe Observer de java.
	 */
	public void update() {
		this.update(this);
	}

}
