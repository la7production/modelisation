package fr.la7prod.sam.gui;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;


public class SMenuBar extends JMenuBar {
	
	private JMenu fichier,edition,sam;
	private JMenuItem ouvrir,importer,importerDossier,enregistrer,enregistrerTout,enregistrerSous,fermer,fermerTout;
	private JMenuItem annuler,retablir;
	private JMenuItem aPropos,masquer,quitter;
	
	public SMenuBar(){
		init();
	}

	private void init() {
	
		//JMenu Fichier
		ouvrir = new JMenuItem("Ouvrir fichier ...");
		importer = new JMenuItem("Importer fichier");
		importerDossier = new JMenuItem("Importer dossier");
		enregistrer = new JMenuItem("Enregistrer");
		enregistrerTout = new JMenuItem("Enregistrer tout");
		enregistrerSous = new JMenuItem("Enregistrer sous ...");
		fermer = new JMenuItem("Fermer");
		fermerTout = new JMenuItem("Fermer tout");
		
		//JMenu Edition
		annuler = new JMenuItem("Annuler");
		retablir = new JMenuItem("Rétablir");
		
		//JMenu S.A.M
		aPropos = new JMenuItem("A Propos de S.A.M");
		masquer = new JMenuItem("Masquer S.A.M");
		quitter = new JMenuItem("Quitter S.A.M");
		
		
		fichier = new JMenu("Fichier");
		edition = new JMenu("Edition");
		sam = new JMenu("S.A.M");
		
		fichier.add(ouvrir);
		fichier.add(importer);
		fichier.add(importerDossier);
		fichier.addSeparator();
		fichier.add(enregistrer);
		fichier.add(enregistrerTout);
		fichier.add(enregistrerSous);
		fichier.addSeparator();
		fichier.add(fermer);
		fichier.add(fermerTout);
		
		edition.add(annuler);
		edition.add(retablir);
		
		sam.add(aPropos);
		sam.addSeparator();
		sam.add(masquer);
		sam.add(quitter);
		
		
		
		this.add(fichier);
		this.add(edition);
		this.add(sam);
		
	}

}
