package fr.la7prod.sam.gui;
import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import fr.la7prod.paintlib.scene.util.Scenes;
import fr.la7prod.sam.gui.spanel.SPanelManager;


public class MainPanel extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 496717405390503183L;
	private JSplitPane split;
	private SPanelManager spanelManager;
	private Scenes scenes;
	
	public MainPanel() {
		init();		
	}

	private void init() {
		
		this.scenes = new Scenes();
		this.spanelManager = new SPanelManager(scenes);
		this.split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,spanelManager,scenes);
		this.setLayout(new BorderLayout());
		this.add(split,BorderLayout.CENTER);
		
			
	}
	

	public SPanelManager getSpanelManager() {
		return spanelManager;
	}

	public Scenes getScenes() {
		return scenes;
	}

}
