package fr.la7prod.sam.gui.light;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import fr.la7prod.paintlib.scene.util.Scenes;
import fr.la7prod.sam.gui.SContentPanel;


public class SLightPanel extends SContentPanel{
	
	private static final long serialVersionUID = -6754838886966589202L;
	
	private SColorChooser colorChoser;
	private JSlider intensity,color;

	public SLightPanel(Scenes scene){
		super(scene);
		this.setPreferredSize(new Dimension(250,350));
		initialize();
	}

	private void initialize() {
		
		this.colorChoser = new SColorChooser();
		this.color = new SColorSlider(JSlider.VERTICAL,0, 255, 100, colorChoser);
		this.intensity = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
		this.intensity.setMajorTickSpacing(100);
		this.intensity.setPaintLabels(true);
		this.intensity.setPaintTicks(true);
		
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new BorderLayout(10,0));
		northPanel.add(colorChoser);
		northPanel.add(color,BorderLayout.WEST);
		JPanel southPanel = new JPanel();
		southPanel.setLayout(new BorderLayout());
		southPanel.add(new JLabel("Intensité",JLabel.CENTER),BorderLayout.NORTH);
		southPanel.add(intensity,BorderLayout.CENTER);
		
		this.setLayout(new BorderLayout(0,10));
		this.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 10));
		this.add(northPanel,BorderLayout.NORTH);
		this.add(southPanel,BorderLayout.SOUTH);
		
		
	}

	public JSlider getColor() {
		return color;
	}

	public SColorChooser getColorChoser() {
		return colorChoser;
	}

	public void setColorChoser(SColorChooser colorChoser) {
		this.colorChoser = colorChoser;
	}

	public JSlider getIntensity() {
		return intensity;
	}

	public void setIntensity(JSlider intensity) {
		this.intensity = intensity;
	}

	public void setColor(JSlider color) {
		this.color = color;
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}
	
	

}