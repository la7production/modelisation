package fr.la7prod.sam.gui.research;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import fr.la7prod.paintlib.scene.util.Scenes;
import fr.la7prod.sam.gui.SContentPanel;


public class SResearchPanel extends SContentPanel implements ActionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8061846594989592301L;
	private JLabel label1;  
	private JLabel label2;
	private JComboBox<String> box;
	private JTextArea area;
	private JButton chercher;
	private JList<String> listRep;
	private GridBagConstraints gb;
	private JPanel p;
	private JPanel pRep;
	
	public SResearchPanel(Scenes scene){
		super(scene);
			
		this.label1 = new JLabel("Rechercher dans : ");
		this.box = new JComboBox<String>(new String[]{"Bibliothèque 1", "Biblio2", "Biblio3", "Tout"});
		this.label2 = new JLabel("Mots clés :");
		this.area = new JTextArea(4, 1);
		
		this.chercher = new JButton("Chercher");
		this.chercher.setFocusable(false);
		this.chercher.addActionListener(this);
		
		this.listRep = new JList<String>(new String[]{"Resultat 1", "Resultat 2", "Resultat 3", "Resultat 4"});
		
		this.p = new JPanel();
		this.pRep = new JPanel();
		this.p.setLayout(new GridBagLayout());
		this.setLayout(new BorderLayout());
		
		this.setBackground(Color.white);
		this.setPreferredSize(new Dimension(250,350));
		
		gb = new GridBagConstraints();
		gb.gridx = 0;
		gb.gridy = 0;
		gb.gridwidth = 2;
		gb.weightx = 0.5;
		gb.insets = new Insets(10, 10, 0, 10);
		p.add(label1,gb);
		
		gb.gridy = 1;
		p.add(box,gb);
		
		gb.gridy = 2;
		p.add(label2,gb);
		
		gb.gridy = 3;
		gb.fill = GridBagConstraints.BOTH;
		p.add(new JScrollPane(area), gb);
		
		gb.gridy = 4;
		p.add(chercher,gb);
		
		this.add(p, BorderLayout.NORTH);
		this.add(pRep, BorderLayout.CENTER);
		
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getSource().equals(chercher)){
			pRep.add(listRep);
			validate();
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}

}
