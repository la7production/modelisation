package fr.la7prod.sam.gui.workspace;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Observable;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import fr.la7prod.paintlib.scene.util.Scenes;
import fr.la7prod.sam.gui.SContentPanel;


public class SWorkspace extends SContentPanel{
	
	private JTree tree;
	
	public SWorkspace(Scenes scene){
		super(scene);
		init();
	}

	private void init() {
		this.setLayout(new BorderLayout());
		this.setBackground(Color.white);
		
		initTree();
		
	}
	
	private void initTree() {
		// TODO Auto-generated method stub
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("NameOfWorkspace");
		tree = new JTree(root);
		tree.setFocusable(false);
		this.createNodes(root);
		this.add(tree,BorderLayout.CENTER);
	}

	private void createNodes(DefaultMutableTreeNode top) {
	    DefaultMutableTreeNode file1,file2,file3,file4;
	    DefaultMutableTreeNode folder1,folder2;
	    file1 = new DefaultMutableTreeNode("config");
	    file2 = new DefaultMutableTreeNode("modele1.gts");
	    file3 = new DefaultMutableTreeNode("modele2.gts");
	    file4 = new DefaultMutableTreeNode("blblbl.xml");
	    folder1 = new DefaultMutableTreeNode("modeles");
	    folder2 = new DefaultMutableTreeNode("dependencies");
	    top.add(file1);
	    top.add(folder1);
	    top.add(folder2);
	    folder1.add(file2);
	    folder1.add(file3);
	    folder2.add(file4);
	    
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}

}
