package fr.la7prod.sam.gui.info;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Observable;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.la7prod.paintlib.model.Model;
import fr.la7prod.paintlib.scene.util.Scenes;
import fr.la7prod.sam.gui.SContentPanel;


public class SInfoPanel extends SContentPanel {
	
	private JPanel panelImage;
	
	private JLabel labelNomFichier,labelDateAjout,labelDateModif,labelCaract;
	private JLabel labelPoints,labelSegments,labelFaces,labelVolume;
	
	
	public SInfoPanel(Scenes scene){
		super(scene);
		init();
	}

	private void init() {
		
		initLabels();
		initPanel();
	
		this.setLayout(new GridBagLayout());
		GridBagConstraints gb = new GridBagConstraints();
		gb.insets = new Insets(20,20,20,20);
		
		gb.gridx = 0;
		gb.gridy = 0;
		gb.fill = GridBagConstraints.BOTH;
		
		
		//Ajout du nom du modèle
		gb.gridwidth = 4;
		gb.gridheight = 1;
		this.add(labelNomFichier,gb);
		
		// Ajout de l'image du modèle
		gb.gridheight = 4;
		gb.gridy = 1;
		this.add(panelImage,gb);
		
		//Ajout de la date d'ajout		
		gb.gridy = 5;
		gb.gridheight = 1;
		this.add(labelDateAjout,gb);
		
		//Ajout de la date de modification
		gb.gridy = 6;
		this.add(labelDateModif,gb);
		
		//Ajout du label "caractéristiques"
		gb.gridy = 7;
		gb.gridwidth = 2;
		this.add(labelCaract,gb);
		
		//Ajout du nombre de points
		gb.gridy = 8;
		this.add(labelPoints,gb);
		
		//Ajout du nombre de faces
		gb.gridx = 2;
		this.add(labelFaces,gb);
			
		//Ajout du nombre de segments
		gb.gridy = 9;
		gb.gridx = 0;
		this.add(labelSegments,gb);
			
		//Ajout du volume
		gb.gridx = 2;
		this.add(labelVolume,gb);
		
			
	}

	private void initLabels() {
		labelNomFichier = new JLabel("ModelStation.gts",JLabel.CENTER);
			labelNomFichier.setFont(new Font(Font.SERIF, Font.BOLD, 24));
		labelDateAjout = new JLabel("Date d'ajout de l'objet : jj/mm/aa",JLabel.CENTER);
		labelDateModif = new JLabel("Dernière modification : jj/mm/aa",JLabel.CENTER);
		labelCaract = new JLabel("Caractéristiques :");
		labelPoints = new JLabel("Points : xxxx");
		labelSegments = new JLabel("Segment : xxxx");
		labelFaces = new JLabel("Faces : xxxx");
		labelVolume = new JLabel("Volume : xxx Mb");
	}
	
	private void initPanel() {
		// TODO Auto-generated method stub
		panelImage = new JPanel();
		panelImage.setBorder(BorderFactory.createTitledBorder("Image"));
	}
	
	public void paintComponent(Graphics g){
		g.setColor(Color.white);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.black);
		g.drawLine(labelNomFichier.getX(), labelNomFichier.getY()+labelNomFichier.getHeight()+5, labelNomFichier.getX()+labelNomFichier.getWidth(), labelNomFichier.getY()+labelNomFichier.getHeight()+5);
		g.drawLine(labelCaract.getX(), labelCaract.getY()+labelCaract.getHeight()+5, labelCaract.getX()+labelCaract.getWidth(), labelCaract.getY()+labelCaract.getHeight()+5);
	}

	@Override
	public void update(Observable obs, Object o) {
		// TODO Auto-generated method stub
		if(o instanceof Scenes){
			Scenes s = ((Scenes)o);
			Model m = s.getCurrentScene().getModel();
			labelNomFichier.setText(m.getName());
			labelPoints.setText("Points : " + m.getVertices().length);
			labelFaces.setText("Faces : " + m.getFaces().length);
			labelSegments.setText("Segments : " + m.getSegments().length);
		}
	}


}
