package fr.la7prod.sam.gui.spanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import fr.la7prod.paintlib.scene.util.Scenes;
import fr.la7prod.sam.gui.action.SActionPanel;
import fr.la7prod.sam.gui.info.SInfoPanel;
import fr.la7prod.sam.gui.light.SLightPanel;
import fr.la7prod.sam.gui.research.SResearchPanel;
import fr.la7prod.sam.gui.workspace.SWorkspace;


public class SPanelManager extends JPanel implements ComponentListener{
	
	private JPanel groupe1,groupe2,onglets1,onglets2;
	private List<SPanel> panels1,panels2;
	private JSplitPane split;	
	
	private int location;	
	private int size;
	
	public static Dimension space = new Dimension(0,306);
	
	private SPanel workspace,research,actions,infos,light;
	
	public SPanelManager(Scenes s){	
		initSPanels(s);
		init();		
	}

	private void init() {
		
		this.panels1 = new ArrayList<SPanel>();		
		this.panels2 = new ArrayList<SPanel>();		
		
		this.groupe1 = new JPanel();
		this.groupe1.setLayout(new BorderLayout());
		
		this.onglets1 = new JPanel();
		this.onglets1.setLayout(new BoxLayout(onglets1, BoxLayout.PAGE_AXIS));
		this.groupe1.add(onglets1,BorderLayout.NORTH);
		
		
		JPanel space1 = new JPanel();
		space1.setPreferredSize(space);
		this.groupe1.add(space1);
		
		
		this.groupe2 = new JPanel();
		this.groupe2.setLayout(new BorderLayout());
		this.onglets2 = new JPanel();
		this.onglets2.setLayout(new BoxLayout(onglets2, BoxLayout.PAGE_AXIS));
		this.groupe2.add(onglets2,BorderLayout.NORTH);
		
		JPanel space2 = new JPanel();
		space2.setPreferredSize(space);
		this.groupe2.add(space2);
		
		
		this.onglets1.add(infos);this.panels1.add(infos);
		this.onglets1.add(actions);this.panels1.add(actions);
		this.onglets1.add(light);this.panels1.add(light);
		
		this.onglets2.add(workspace);this.panels2.add(workspace);
		this.onglets2.add(research);this.panels2.add(research);
		
		
	
		split = new JSplitPane(JSplitPane.VERTICAL_SPLIT, groupe1, groupe2);
		this.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		this.setBackground(Color.white);
		this.setLayout(new BorderLayout());
		this.add(split,BorderLayout.CENTER);
		
		// taille optimale = space.height + nbSPanel * SPanel.height + 7
		this.location = space.height + panels1.size() * SPanel.headerSize.height + 7; 		
		this.split.setDividerLocation(location);
		this.addComponentListener(this);
		this.groupe2.addComponentListener(this);
	}

	private void initSPanels(Scenes scene) {
		
		workspace = new SPanel("Workspace",this);
		research = new SPanel("Recherche critères",this);
		infos = new SPanel("Informations objet",this);
		actions = new SPanel("Action sur l'objet",this);
		light = new SPanel("Lumiere",this);
		
		workspace.setContent(new SWorkspace(scene));
		research.setContent(new SResearchPanel(scene));
		infos.setContent(new SInfoPanel(scene));
		actions.setContent(new SActionPanel(scene));
		light.setContent(new SLightPanel(scene));
		
	}
	
	public void press(SPanel panel){
		
		int diff = split.getDividerLocation()-location;
		
		if(diff != 0){
			for(SPanel p : panels1){
				p.agrandir(diff);
			}
			for(SPanel p : panels2){
				p.agrandir(-diff);
			}
		}
		
		location = split.getDividerLocation();
		
		
		List<SPanel> list;
		
		if(panels1.contains(panel)){
			list = panels1;
		}else{
			list = panels2;
		}
		
		if(panel.isActivated()){
			panel.deactivate();
		}else{
			for(SPanel s : list)
				s.deactivate();
			panel.activate();
		}
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
	
	}

	@Override
	public void componentResized(ComponentEvent arg0) {
		if(arg0.getSource().equals(this)){
			if(this.size <=0){
				this.size = this.getHeight();
			}	
			int diff = this.getHeight()-size;
			this.split.setDividerLocation(this.split.getDividerLocation()+diff);
			size = this.getHeight();
			for(SPanel s : panels2){
				s.agrandir(diff);
			}
		}else if(arg0.getSource().equals(groupe2)){
			int diff = split.getDividerLocation()-location;
			
			System.out.println(diff);
			
			if(diff != 0){
				for(SPanel p : panels1){
					p.agrandir(diff);
				}
				for(SPanel p : panels2){
					p.agrandir(-diff);
				}
			}
			
			location = split.getDividerLocation();
			
			for(SPanel panel : panels1){
				if(panel.isActivated()){
					panel.deactivate();
					panel.activate();
				}
			}
			
			for(SPanel panel : panels2){
				if(panel.isActivated()){
					panel.deactivate();
					panel.activate();
				}
			}
			
			
			
		}
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		// TODO Auto-generated method stub
		
	}


}
