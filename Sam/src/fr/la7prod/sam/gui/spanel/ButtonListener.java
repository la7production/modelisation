package fr.la7prod.sam.gui.spanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class ButtonListener implements ActionListener {
	
	private SPanel panel;
	
	public ButtonListener(SPanel panel){
		this.panel = panel;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getSource().equals(panel.getButton())){
			panel.getManager().press(panel);
		}
	}

}
