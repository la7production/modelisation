package fr.la7prod.sam.gui.spanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class SPanel extends JPanel {
	
	private SPanelManager spanelManager;
	
	private JPanel header;
	private JPanel content;
	private JButton button;
	private JLabel title;
	
	//La taille en hauteur dépendra de la place disponible dans le groupe du SPanel. La taille en largeur n'est pas à préciser, 
	//le composant content étant ajouté en BorderLayout.CENTER
	private Dimension space;
	public final static Dimension headerSize = new Dimension(330,30);
	private boolean activated;
	
	public SPanel(SPanelManager sm) {
		this.spanelManager = sm;
		init();
	}
	
	public SPanel(String texte,SPanelManager sm){
		this.spanelManager = sm;
		init();
		title.setText(texte);
	}

	private void init() {
		this.header = new JPanel();
		this.header.setPreferredSize(headerSize);
		this.header.setBorder(BorderFactory.createLineBorder(Color.darkGray,1));
		this.header.setLayout(new BorderLayout());
			this.title = new JLabel("SPanel",JLabel.CENTER);
			this.button = new JButton("▼");
			this.button.setContentAreaFilled(false);
			this.button.setFocusable(false);
			this.button.addActionListener(new ButtonListener(this));
			this.header.add(title);
			this.header.add(button,BorderLayout.EAST);
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createLineBorder(Color.black,1));
		this.add(header,BorderLayout.NORTH);
		
		this.space = SPanelManager.space;
		
		this.content = new JPanel();	
		this.content.setPreferredSize(space);
		this.content.setBackground(Color.lightGray);
		this.content.setLayout(new BorderLayout());
		
		
	}
	
	public JButton getButton(){
		return this.button;
	}
	
	public boolean isActivated() {
		return activated;
	}

	public void activate(){
		activated = true;
		this.button.setText("▲");
		this.add(content);
	}
	
	public void deactivate(){
		this.activated = false;
		this.button.setText("▼");
		this.remove(content);
	}
	
	public SPanelManager getManager(){
		return this.spanelManager;
	}
	
	public void agrandir(int size){
		this.content.setPreferredSize(new Dimension(0,this.space.height+size));
		this.space = new Dimension(0,this.space.height+size);
	}
	
	public int getH(){
		return this.space.height;
	}
	
	public JPanel getContent(){
		return this.content;
	}

	public void setContent(JPanel panel) {
		this.content.removeAll();
		this.content.add(new JScrollPane(panel),BorderLayout.CENTER);
	}
	
}
