package fr.la7prod.sam.gui.action;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Observable;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import fr.la7prod.paintlib.scene.util.Scenes;
import fr.la7prod.sam.gui.SContentPanel;


public class SActionPanel extends SContentPanel{
	
	private JButton centerT;

	public SActionPanel(Scenes scene){
		super(scene);
		this.setPreferredSize(new Dimension(250,350));
		initialisation();
	}
	
	public void initialisation(){
		this.setLayout(new GridLayout(2, 0));
		
		JLabel labelT = new JLabel("TRANSLATION :",JLabel.CENTER);
		JLabel labelR = new JLabel("ROTATION :",JLabel.CENTER);
		JLabel labelTX = new JLabel("Axe X  ");
		JLabel labelTY = new JLabel("Axe Y  ");
		JLabel labelTZ = new JLabel("Axe Z  ");
		JLabel labelRX = new JLabel("Angle X  ");
		JLabel labelRY = new JLabel("Angle Y  ");
		JLabel labelRZ = new JLabel("Angle Z  ");
		labelR.setPreferredSize(new Dimension(0,30));
		labelT.setFont(new Font("", Font.BOLD, 20));
		labelR.setFont(new Font("", Font.BOLD, 20));
		
		JSlider sliderRotateTX = new JSlider(0, 359,0);
		JSlider sliderRotateTY = new JSlider(0, 359,0);
		JSlider sliderRotateTZ = new JSlider(0, 359,0);
		JSlider sliderRotateRX = new JSlider(0, 359,0);		
		JSlider sliderRotateRY = new JSlider(0, 359,0);
		JSlider sliderRotateRZ = new JSlider(0, 359,0);
		
		centerT = new JButton("Centrer");
		JButton reinitT = new JButton("Réinitialiser");
		JButton centerR = new JButton("Centrer");
		JButton reinitR = new JButton("Réinitialiser");
		centerT.setFocusable(false);
		reinitT.setFocusable(false);
		centerR.setFocusable(false);
		reinitR.setFocusable(false);
		
		
		/* Panel Translation */
		JPanel panelTranslation = new JPanel();
		JPanel panelSliderT = new JPanel();
		JPanel panelLabelT = new JPanel();
		JPanel panelTop = new JPanel();
		JPanel panelButtonT = new JPanel();
		
		/* Panel Rotation */
		JPanel panelRotation = new JPanel();
		JPanel panelSliderR = new JPanel();
		JPanel panelLabelR = new JPanel();
		JPanel panelBot = new JPanel();
		JPanel panelButtonR = new JPanel();
		
		/*Layout panel translation */
		panelTranslation.setLayout(new BoxLayout(panelTranslation, BoxLayout.LINE_AXIS));
		panelLabelT.setLayout(new BoxLayout(panelLabelT, BoxLayout.PAGE_AXIS));
		panelSliderT.setLayout(new BoxLayout(panelSliderT, BoxLayout.PAGE_AXIS));
		panelButtonT.setLayout(new FlowLayout());
		panelTop.setLayout(new BorderLayout());
		
		/*Layout panel Rotation */
		panelRotation.setLayout(new BoxLayout(panelRotation, BoxLayout.LINE_AXIS));
		panelLabelR.setLayout(new BoxLayout(panelLabelR, BoxLayout.PAGE_AXIS));
		panelSliderR.setLayout(new BoxLayout(panelSliderR, BoxLayout.PAGE_AXIS));
		panelButtonR.setLayout(new FlowLayout());
		panelBot.setLayout(new BorderLayout());
		
		/*add panel Translation*/
		panelLabelT.add(labelTX);
		panelSliderT.add(sliderRotateTX);
		panelLabelT.add(labelTY);
		panelSliderT.add(sliderRotateTY);
		panelLabelT.add(labelTZ);
		panelSliderT.add(sliderRotateTZ);
		panelButtonT.add(centerT);
		panelButtonT.add(reinitT);
		
		/*add panel Rotation*/
		panelLabelR.add(labelRX);
		panelSliderR.add(sliderRotateRX);
		panelLabelR.add(labelRY);
		panelSliderR.add(sliderRotateRY);
		panelLabelR.add(labelRZ);
		panelSliderR.add(sliderRotateRZ);
		panelButtonR.add(centerR);
		panelButtonR.add(reinitR);
		
		/*add panel Top*/
		panelTranslation.add(panelLabelT);
		panelTranslation.add(panelSliderT);
		panelTop.add(labelT, BorderLayout.NORTH);
		panelTop.add(panelTranslation, BorderLayout.CENTER);
		panelTop.add(panelButtonT, BorderLayout.SOUTH);
		
		/*add panel Bot*/
		panelRotation.add(panelLabelR);
		panelRotation.add(panelSliderR);
		panelBot.add(labelR, BorderLayout.NORTH);
		panelBot.add(panelRotation, BorderLayout.CENTER);
		panelBot.add(panelButtonR, BorderLayout.SOUTH);
		
		this.add(panelTop);
		this.add(panelBot);
		
		
	}
	
	
	@Override
	public void update(Observable obs,Object o){
		
	}

}
