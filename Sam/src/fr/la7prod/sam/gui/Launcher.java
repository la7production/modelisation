package fr.la7prod.sam.gui;
import javax.swing.JFrame;


public class Launcher extends JFrame{

	public Launcher(){
		
		this.setJMenuBar(new SMenuBar());
		this.setContentPane(new MainPanel());
		
		this.setVisible(true);
		this.setResizable(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(null);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Launcher();

	}

}
