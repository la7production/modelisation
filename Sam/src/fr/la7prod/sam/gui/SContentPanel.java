package fr.la7prod.sam.gui;

import java.util.Observer;

import javax.swing.JPanel;

import fr.la7prod.paintlib.scene.util.Scenes;

public abstract class SContentPanel extends JPanel implements Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 681297256425112740L;
	
	//private Updatable updatable;
	private Scenes scenes;
	
	public SContentPanel(Scenes scenes){
		this.scenes = scenes;
		this.scenes.addObserver(this);
	}

	public Scenes getScenes(){
		return this.scenes;
	}
	
	
}
