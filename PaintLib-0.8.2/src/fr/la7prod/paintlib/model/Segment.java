/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.model;

import fr.la7prod.paintlib.model.util.Identifiable;
import fr.la7prod.paintlib.model.util.Vector3D;

/**
 * Un segment est caractérisé par deux points et un identifiant
 * qui sera utilisé par ses faces associées dans le fichier .gts
 */
public class Segment implements Identifiable {
	
	public static int NBINSTANCES = 0;
	public static final int NBPOINTS = 2;
	
	private int id;
	private Point3D p1;
	private Point3D p2;
	
	public Segment(Point3D p1, Point3D p2) {
		this.id = ++NBINSTANCES;
		this.p1 = p1;
		this.p2 = p2;
	}
	
	@Override
	public int getNumero() {
		return this.id;
	}
	
	/**
	 * Retourne le premier point du segment noté A
	 * @return le premier point du segment
	 */
	public Point3D getA() {
		return this.p1;
	}
	
	/**
	 * Retourne le second point du segment noté B
	 * @return le second point du segment
	 */
	public Point3D getB() {
		return this.p2;
	}
	
	/**
	 * Retourne le vecteur obtenu à partir du segment
	 * @return le vecteur obtenu grâce à la soustraction des deux points du segment
	 */
	public Vector3D getVector() {
		return new Vector3D(
				p2.getX()-p1.getX(),
				p2.getY()-p1.getY(),
				p2.getZ()-p1.getZ());
	}
	
	@Override
	public String toString() {
		return String.format("Segment:\n%s\n%s", p1, p2);
	}

}
