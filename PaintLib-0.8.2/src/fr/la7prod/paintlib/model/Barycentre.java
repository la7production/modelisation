/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.model;

import java.util.Comparator;

import fr.la7prod.paintlib.model.util.Modelisable;
import fr.la7prod.paintlib.model.util.Vector3D;

/**
 * Le Barycentre d'un objet modélisable est son centre de gravité
 * @see fr.la7prod.paintlib.model.util.Modelisable
 */
public class Barycentre extends Point3D implements Comparable<Barycentre> {
	
	private Modelisable model;
	private Barycentre save;
	
	public Barycentre(Modelisable model) {
		this.model = model;
		this.save = new Barycentre(this);
	}
	
	private Barycentre() {}
	
	private Barycentre(Barycentre toSave) {
		this.save = new Barycentre();
		this.save.model = toSave.model;
	}
	
	/**
	 * Retourne le modèle associé au barycentre courant
	 * @return le modèle associé au barycentre
	 */
	public Modelisable getParent() {
		return this.model;
	}
	
	/**
	 * Calcule le barycentre d'un polygone.
	 * Les mouvements (translations et rotations) d'un point
	 * s'appliquent aux barycentres.
	 */
	public void calculate() {
		Point3D p = new Point3D(0,0,0);
		Point3D[] vertices = this.getParent().getVertices();
		for(Point3D point : vertices) {
			p.translate(new Vector3D(point.getX(),point.getY(), point.getZ()));
		}
		this.move(p.getX()/vertices.length,p.getY()/vertices.length,p.getZ()/vertices.length);
	}
	
	/**
	 * Sauvegarde la position du barycentre malgré les mouvements de la figure.
	 * Les prochains mouvements suivront donc le nouveau barycentre.
	 */
	public void save() {
		save.move(this.getX(), this.getY(), this.getZ());
	}
	
	/**
	 * Restaure la position du barycentre suite à une sauvegarde de celui-ci.
	 */
	public void restore() {
		this.translateModel(save.getX(),save.getY(),save.getZ());
	}
	
	/**
	 * Déplace les coordonnées du barycentre
	 * @param x la coordonnée x du barycentre
	 * @param y la coordonnée y du barycentre
	 * @param z la coordonnée z du barycentre
	 */
	public void translateModel(double x, double y, double z) {
		this.getParent().translate(new Vector3D(x-this.getX(),y-this.getY(), z-this.getZ()));
	}
	
	/**
	 * Effectue la comparaison d'un barycentre avec le barycentre courant, selon un comparateur précis
	 * @param comparator le comparator personnalisé permettant de comparer deux barycentres
	 * @param b le barycentre comparé au barycentre courant
	 * @return l'entier retourné par la méthode de comparaison du comparator
	 */
	public int compareWith(Comparator<Barycentre> comparator, Barycentre b) {
		return comparator.compare(this, b);
	}
	
	@Override
	public int compareTo(Barycentre b) {
		if (this.getZ() < b.getZ())
			return -1;
		if (this.getZ() == b.getZ())
			return 0;
		if (this.getZ() > b.getZ())
			return 1;
		throw new IllegalArgumentException();
	}
	
	@Override
	public String toString() {
		return "Barycentre: " + super.toString();
	}
	

}
