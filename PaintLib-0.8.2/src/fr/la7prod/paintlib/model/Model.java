/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.model;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.la7prod.paintlib.gui.scene.SpotLight;
import fr.la7prod.paintlib.model.util.Modelisable;

/**
 * Un model est un ensemble de faces, de segments et de points
 * avec son propre barycentre. C'est une forme géométrique qui peut
 * subir des mouvements (translation, rotation, zoom).
 * @see fr.la7prod.paintlib.model.util.Modelisable
 */
public class Model extends Modelisable {
	
	private Face[] faces;
	private List<Barycentre> baries;
	private boolean bariesSorted;
	
	public Model(Face[] faces, Segment[] segments, Point3D[] points) {
		super();
		this.setSegment(segments);
		this.setVertices(points);
		this.faces = faces;
		this.baries = new ArrayList<Barycentre>(faces.length);
		for(Face f : faces) {
			baries.add(f.getBarycentre());
		}
		this.bariesSorted = false;
		this.getBarycentre().calculate();
		this.getBarycentre().save();
	}
	
	/**
	 * Retourne toutes les faces constituant la figure
	 * @return le tableau de toutes les faces de la figure
	 */
	public Face[] getFaces() {
		return this.faces;
	}
	
	@Override
	public void rotate(double rx, double ry, double rz) {
		super.rotate(rx, ry, rz);
		bariesSorted = false;
	}
	
	@Override
	public void draw(Graphics g, SpotLight spot, boolean filled) {
		for(Barycentre b: baries) {
			b.calculate();
		}
		if (!bariesSorted) {
			Collections.sort(baries);
			bariesSorted = true;
		}
		for(Barycentre b : baries) {
			b.getParent().draw(g, spot, filled);
		}
	}
	
	@Override
	public String toString() {
		String str = "";
		for(Face f : faces)
			str += " " + f.toString() + "\n";
		return "Figure composée des faces:\n" + str;
	}

}
