/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import fr.la7prod.paintlib.gui.scene.SpotLight;
import fr.la7prod.paintlib.model.util.Modelisable;

/**
 * Une face est un triangle composé de 3 points communs à 3 segments
 * considéré comme un objet Modelisable
 * @see fr.la7prod.paintlib.model.util.Modelisable
 */
public class Face extends Modelisable {

	public static final int NBPOINTS = 3;
	private Polygon triangle;
	private Color color;
	
	public Face(Segment s1, Segment s2, Segment s3) {
		super();
		this.triangle = new Polygon();
		this.triangle.npoints = NBPOINTS;
		this.triangle.xpoints = new int[NBPOINTS];
		this.triangle.ypoints = new int[NBPOINTS];
		this.setSegment(new Segment[]{s1,s2,s3});
		this.setVertices(commonVertices());
		this.getBarycentre().calculate();
		this.getBarycentre().save();
	}
	
	/**
	 * Trouve les points communs des 3 segments composant la face
	 * On considère de ce fait que les 3 segments ont forcément
	 * un point en commun avec l'un ou l'autre des 2 autres segments
	 * Ces points sont en réalité les sommets de la face
	 * @return le tableau contenant les sommets de la face
	 */
	private Point3D[] commonVertices() {
		Segment s1 = this.getSegment(0);
		Segment s2 = this.getSegment(1);
		Point3D[] points = new Point3D[NBPOINTS];
		points[0] = s1.getA();
		points[1] = s1.getB();
		if (s2.getA().equals(points[0]) || s2.getA().equals(points[1]))
			points[2] = s2.getB();
		else
			points[2] = s2.getA();
		return points;
	}
	
	/**
	 * Retourne la couleur de la face
	 * @return la couleur de la face
	 */
	public Color getColor() {
		return this.color;
	}
	
	/**
	 * Saisie une nouvelle couleur pour la face
	 * @param color la nouvelle couleur de la face
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	/**
	 * Indique si un point est contenu dans la face
	 * @param p le point contenu dans la face ou non
	 * @return vrai si le point est contenu dans la face
	 */
	public boolean contains(Point3D p) {
		return triangle.contains(p.getX(), p.getY());
	}
	
	@Override
	public void draw(Graphics g, SpotLight spot, boolean filled) {
		for(int i=0; i<NBPOINTS; i++) {
			triangle.xpoints[i] = (int)this.getVertex(i).getX();
			triangle.ypoints[i] = (int)this.getVertex(i).getY();
		}
		spot.setColorOf(this);
		g.setColor(color);
		if (filled)
			g.fillPolygon(triangle);
		else
			g.drawPolygon(triangle);
	}
	
	@Override
	public String toString() {
		Point3D[] vertex = this.getVertices();
		return String.format("Face:\n%s\n%s\n%s",vertex[0],vertex[1], vertex[2]);
	}

}
