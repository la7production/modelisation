/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.model.util;

/**
 * Identifiable est une interface qui permet
 * d'implémenter un système de numérotation d'objet.
 * Il est utilisé par exemple pour Point et Segment afin de leur
 * donner des numéros d'identification qui seront utilisés dans les fichiers .gts
 */
public interface Identifiable {
	
	/**
	 * Retourne le numéro d'identifiant d'un objet identifiable
	 * @return un entier correspondant à l'identifiant d'un objet identifiable
	 */
	public int getNumero();

}
