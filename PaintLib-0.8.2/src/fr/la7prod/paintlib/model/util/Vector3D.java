/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.model.util;

import fr.la7prod.paintlib.model.Face;
import fr.la7prod.paintlib.model.Point3D;

/**
 * En mathématiques, un vecteur est un élément d'un espace vectoriel,
 * ce qui permet d'effectuer des opérations d'addition et de multiplication par un scalaire.
 */
public class Vector3D implements Cloneable, Movable {
	
	private double x;
	private double y;
	private double z;
	
	public Vector3D(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Vector3D(Vector3D u, Vector3D v) {
		this(v.x-u.x, v.y-u.y, v.z-u.z);
	}
	
	public Vector3D(Point3D p1, Point3D p2) {
		this(p2.getX()-p1.getX(),p2.getY()-p1.getY(),p2.getZ()-p1.getZ());
	}
	
	/**
	 * Retourne l'ordonnée (profondeur) du vecteur de coordonnées x, y, z
	 * @return la coordonnée y
	 */
	public double getX() {
		return this.x;
	}
	
	/**
	 * Modifie l'abscisse du vecteur de coordonnées x, y, z 
	 * @param x la coordonnée à modifier
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Retourne l'ordonnée (profondeur) du vecteur de coordonnées x, y, z
	 * @return la coordonnée y
	 */
	public double getY() {
		return this.y;
	}

	/**
	 * Modifie l'ordonnée (profondeur) du vecteur de coordonnées x, y, z
	 * @param y la coordonnée à modifier
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * Retourne la hauteur du vecteur de coordonnées x, y, z
	 * @return la coordonnée z
	 */
	public double getZ() {
		return this.z;
	}
	
	/**
	 * Modifie la hauteur du vecteur de coordonnées x, y, z
	 * @param z la coordonnée à modifier
	 */
	public void setZ(double z) {
		this.z = z;
	}
	
	/**
	 * Change la direction du vecteur courant via un autre vecteur
	 * @param v le vecteur pour lesquelles ses valeurs vont être prises par le vecteur courant
	 */
	public void changeTo(Vector3D v) {
		this.x = v.x;
		this.y = v.y;
		this.z = v.z;
	}

	/**
	 * Retourne la norme du vecteur
	 * @return la norme sous forme de double du vecteur
	 */
	public double normalize() {
		return Math.sqrt((x*x) + (y*y) + (z*z));
	}
	
	/**
	 * Retourne le vecteur normal unitaire du vecteur
	 * @return le vecteur normal unitaire du vecteur
	 */
	public Vector3D utilNormalVector() {
		Vector3D util = this.clone();
		util.zoom(1.0d / normalize());
		return util;
	}
	
	/**
	 * Retourne le vecteur inverse du vecteur courant
	 * @return le vecteur inverse (coordonnées = - coordonnées) du vecteur
	 */
	public Vector3D reverse() {
		return new Vector3D(-x,-y,-z);
	}
	
	/**
	 * Retourne l'angle formé par le vecteur et l'axe x
	 * @return la valeur de l'angle formé par le vecteur et l'axe x
	 */
	public double getAngleX() {
		return Math.acos(scalar(this, new Vector3D(1,0,0)) / this.normalize());
	}
	
	/**
	 * Retourne l'angle formé par le vecteur et l'axe y
	 * @return la valeur de l'angle formé par le vecteur et l'axe y
	 */
	public double getAngleY() {
		return Math.acos(scalar(this, new Vector3D(0,1,0)) / this.normalize());
	}
	
	/**
	 * Retourne l'angle formé par le vecteur et l'axe z
	 * @return la valeur de l'angle formé par le vecteur et l'axe z
	 */
	public double getAngleZ() {
		return Math.acos(scalar(this, new Vector3D(0,0,1)) / this.normalize());
	}
	
	@Override
	public void move(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public void move(double x, double y, double z) {
		this.move(x,y);
		this.z = z;
	}

	@Override
	public void translate(Vector3D v) {
		this.x += v.getX();
		this.y += v.getY();
		this.z += v.getZ();		
	}

	@Override
	public void zoom(double value) {
		this.x *= value;
		this.y *= value;
		this.z *= value;
	}
	
	@Override
	public void rotate(double rx, double ry, double rz) {
		Point3D p = new Point3D(x,y,z);
		p.rotate(rx, ry, rz);
		this.changeTo(p.toVector());
	}
	
	/**
	 * Retourne vrai si deux points sont identiques
	 * @param v le point à comparer avec le point courant
	 * @return vrai si les deux points sont identiques
	 */
	private boolean equals(Vector3D v) {
		return x == v.x && y == v.y && z == v.z;
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof Vector3D) && equals((Vector3D)o);
	}
	
	@Override
	public String toString(){
		return String.format("Vector(x:%s, y:%s, z:%s)", x,y,z);
	}
	
	@Override
	public Vector3D clone() {
		return new Vector3D(x,y,z);
	}
	
	/**
	 * Calcul le produit vectoriel de deux vecteurs
	 * @param u le premier vecteur
	 * @param v le second vecteur
	 * @return le vecteur créé par le produit vectoriel des deux vecteurs passés en paramètres
	 */
	public static Vector3D crossProduct(Vector3D u, Vector3D v) {
		return new Vector3D(
				u.y*v.z - u.z*v.y,
				u.z*v.x - u.x*v.z,
				u.x*v.y - u.y*v.x);
	}
	
	/**
	 * Retourne le vecteur normal d'une face
	 * @param face la face pour laquelle on cherche son vecteur normal
	 * @return le vecteur normal de la face passée en paramètre
	 */
	public static Vector3D normal(Face face) {
		Vector3D u = face.getSegment(0).getVector();
		Vector3D v = face.getSegment(1).getVector();
		return Vector3D.crossProduct(u, v);
	}
	
	/**
	 * Retourne le produit scalaire de deux vecteurs
	 * @param u le premier vecteur
	 * @param v le second vecteur
	 * @return le produit scalaire des deux vecteurs passés en paramètres
	 */
	public static double scalar(Vector3D u, Vector3D v) {
		return u.x*v.x + u.y*v.y + u.z*v.z;
	}
	
	/**
	 * Retourne le cosinus obtenu grâce à deux vecteurs
	 * @param u le premier vecteur
	 * @param v le second vecteur
	 * @return le cosinus obtenu via les deux vecteurs passés en paramètres
	 */
	public static double cos(Vector3D u, Vector3D v) {
		return Math.abs(Vector3D.scalar(u, v) / (u.normalize() * v.normalize()));
	}

}
