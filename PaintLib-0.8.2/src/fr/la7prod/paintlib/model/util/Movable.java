/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.model.util;

/**
 * Un objet Movable est un objet pouvant subir différents des mouvements.
 * Dans notre cas, il est coupable à Modelisable pour créer des modèles
 */
public interface Movable {
	
	/**
	 * Déplace un objet movable en modifiant directement ses coordonnées
	 * @param x la nouvelle coordonnée x
	 * @param y la nouvelle coordonnée y
	 */
	public void move(double x, double y);
	
	/**
	 * Déplace un objet movable en modifiant directement ses coordonnées
	 * @param x la nouvelle coordonnée x
	 * @param y la nouvelle coordonnée y
	 * @param z la nouvelle coordonnée z
	 */
	public void move(double x, double y, double z);
	
	/**
	 * Effectue une translation de l'objet movable
	 * @param v le vecteur de translation
	 */
	public void translate(Vector3D v);
	
	/**
	 * Effectue un agrandissement de l'objet movable
	 * Note: le barycentre de l'objet movable reste au même endroit,
	 * une translation est donc effectuée après que le zoom eut été fait.
	 * @param value le nombre de pixels d'agrandissement
	 */
	public void zoom(double value);
	
	/**
	 * Effectue la rotation des points d'un objet movable
	 * @param rx l'angle pour la coordonnée x
	 * @param ry l'angle pour la coordonnée y
	 * @param rz l'angle pour la coordonnée z
	 */
	public void rotate(double rx, double ry, double rz);

}
