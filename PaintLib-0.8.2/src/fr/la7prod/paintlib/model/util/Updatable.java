/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.model.util;

import java.util.Observable;

/**
 * Un objet dit Updatable (qui peut être mis à jour) est un objet Observable
 * possédant une méthode update() qui permet d'indiquer à ses observeurs
 * lorsqu'il subit un changement d'état ou des modifications.
 */
public abstract class Updatable extends Observable {
	
	/**
	 * Indique que l'objet updatable a subit une modification, un changement d'état
	 * et permet de prévenir les observeurs de l'objet des changements effectués.
	 * Cela aura pour conséquence d'appeler la méthode update() de tous les observeurs de la vue.
	 * Note: Les observeurs étendent la classe Observer de java.
	 */
	public void update() {
		this.setChanged();
		this.notifyObservers();
	}

}
