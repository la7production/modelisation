/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.model.util;

import fr.la7prod.paintlib.model.Model;

/**
 * AutoRotate permet d'effectuer une rotation
 * en continue de la figure voulue.
 * La figure souhaité se trouve généralement dans une vue.
 * Ici on considère qu'une vue peut changer de figure.
 * 
 * Cette classe est deprecated car elle vise à disparaître
 * lorsque l'interface utilisateur sera créé.
 * L'utilisateur pourra choisir d'effectuer une rotation automatique
 * de la figure qu'il observe, mais cela sera effectué différemment.
 */
@Deprecated
public class AutoRotate extends Thread {
	
	public static int SLEEP = 50;
	
	private Model model;
	
	public AutoRotate(Model model) {
		this.model = model;
	}
	
	public void setModel(Model model) {
		this.model = model;
	}
	
	public void run(){
		while(true){
			try {
				Thread.sleep(SLEEP);
				model.rotate(Math.PI/2,Math.PI/2,0);
				model.update();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
