/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.model;

import fr.la7prod.paintlib.model.util.Identifiable;
import fr.la7prod.paintlib.model.util.Movable;
import fr.la7prod.paintlib.model.util.Vector3D;

/**
 * Un point est un ensemble de coordonnées dans un espace en 3 dimensions
 * Il a également un numéro d'identifiant pour être reconnu par les segments
 * dans le fichier .gts
 */
public class Point3D implements Cloneable, Identifiable, Movable {
	
	public static int NBINSTANCES = 0;
	public static int DPD = 50;
	
	private int id;
	private double x;
	private double y;
	private double z;
	
	public Point3D() {}
	
	public Point3D(double x, double y, double z) {
		this.id = ++NBINSTANCES;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	@Override
	public int getNumero() {
		return this.id;
	}
	
	/**
	 * Retourne l'abscisse d'un point de coordonnées x, y, z
	 * @return la coordonnée x
	 */
	public double getX() {
		return x;
	}

	/**
	 * Modifie l'abscisse d'un point de coordonnées x, y, z 
	 * @param x la coordonnée à modifier
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Retourne l'ordonnée (profondeur) d'un point de coordonnées x, y, z
	 * @return la coordonnée y
	 */
	public double getY() {
		return y;
	}

	/**
	 * Modifie l'ordonnée (profondeur) d'un point de coordonnées x, y, z
	 * @param y la coordonnée à modifier
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * Retourne la hauteur d'un point de coordonnées x, y, z
	 * @return la coordonnée z
	 */
	public double getZ() {
		return z;
	}
	
	/**
	 * Modifie la hauteur d'un point de coordonnées x, y, z
	 * @param z la coordonnée à modifier
	 */
	public void setZ(double z) {
		this.z = z;
	}
	
	@Override
	public void move(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public void move(double x, double y, double z) {
		this.move(x,y);
		this.z = z;
	}
	
	@Override
	public void translate(Vector3D v) {
		this.x += v.getX();
		this.y += v.getY();
		this.z += v.getZ();
	}
	
	/**
	 * Effectue une rotation du point par rapport à la coordonnée x
	 * @param angle l'angle de rotation du point
	 */
	private void rotateX(double angle) {
		double y = this.y;
		double z = this.z;
		angle /= DPD;
		this.y = (y * Math.cos(angle) - z * Math.sin(angle));
		this.z = (y * Math.sin(angle) + z * Math.cos(angle));
	}
	
	/**
	 * Effectue une rotation du point par rapport à la coordonnée y
	 * @param angle l'angle de rotation du point
	 */
	private void rotateY(double angle) {
		double x = this.x;
		double z = this.z;
		angle /= DPD;
		this.x = (x * Math.cos(angle) + z * Math.sin(angle));
		this.z = (-x * Math.sin(angle) + z * Math.cos(angle));
	}
	
	/**
	 * Effectue une rotation du point par rapport à la coordonnée z
	 * @param angle l'angle de rotation du point
	 */
	private void rotateZ(double angle) {
		double x = this.x;
		double y = this.y;
		angle /= DPD;
		this.x = (x * Math.cos(angle) - y * Math.sin(angle));
		this.y = (x * Math.sin(angle) + y * Math.cos(angle));
	}
	
	@Override
	public void rotate(double rx, double ry, double rz) {
		rotateX(rx);
		rotateY(ry);
		rotateZ(rz);
	}
	
	@Override
	public void zoom(double value) {
		this.x *= value;
		this.y *= value;
		this.z *= value;
	}
	
	public Vector3D toVector() {
		return new Vector3D(x,y,z);
	}
	
	/**
	 * Retourne vrai si deux points sont identiques
	 * @param p le point à comparer avec le point courant
	 * @return vrai si les deux points sont identiques
	 */
	private boolean equals(Point3D p) {
		return x == p.x && y == p.y && z == p.z;
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof Point3D) && equals((Point3D)o);
	}
	
	@Override
	public String toString() {
		return String.format("Point(x:%s, y:%s, z:%s)", x,y,z);
	}
	
	@Override
	public Point3D clone() {
		return new Point3D(x,y,z);
	}
	
}
