/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.gts;

import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

import javax.swing.JFileChooser;

import fr.la7prod.paintlib.model.Model;

/**
 * GtsCaller permet d'afficher une boite de dialogue
 * afin de charger ou sauvegarder un fichier au format .gts
 */
public class GtsCaller {
	
	private JFileChooser chooser;
	
	public GtsCaller() {
		this.chooser = new JFileChooser();
		this.chooser.setFileFilter(new GtsFilter());
	}
	
	/**
	 * Ouvre un fichier de figure via une boite de dialogue
	 * @return une figure géométrique
	 * @throws IOException le fichier est corrompu
	 * @throws NoSuchElementException le fichier n'est pas un fichier de figure
	 */
	public Model open() throws IOException, NoSuchElementException {
		File file;
		chooser.showOpenDialog(null);
		file = chooser.getSelectedFile();
		return GtsFactory.loadModel(file);
	}
	
	/**
	 * Sauvegarde un fichier de figure via une boite de dialogue
	 * @param figure la figure géométrique à sauvegarder
	 * @throws IOException le fichier n'a pas pu être créé
	 */
	public void saveAs(Model figure) throws IOException {
		File file;
		chooser.showSaveDialog(null);
		file = chooser.getSelectedFile();
		GtsFactory.saveModel(file, figure);
	}

}
