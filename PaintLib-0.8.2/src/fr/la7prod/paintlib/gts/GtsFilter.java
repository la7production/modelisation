/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.gts;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * GtsFilter permet de filtrer les informations
 * de FileEditor. On ne conserve que les dossiers
 * et les fichiers au format .gts
 */
public class GtsFilter extends FileFilter {

	@Override
	public boolean accept(File f) {
		return f.isDirectory() || f.getName().endsWith(".gts");
	}

	@Override
	public String getDescription() {
		return "GNU Triangulated Surface (*.gts)";
	}
	
	

}
