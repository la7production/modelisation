/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.gts;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.NoSuchElementException;
import java.util.Scanner;

import fr.la7prod.paintlib.model.Face;
import fr.la7prod.paintlib.model.Model;
import fr.la7prod.paintlib.model.Point3D;
import fr.la7prod.paintlib.model.Segment;

/**
 * GtsFactory est une "usine" permettant de charger
 * ou sauvegarder une figure à partir d'un fichier .gts
 */
public class GtsFactory {
	
	/**
	 * Retourne l'indice d'un tableau via un nombre dans en fichier.
	 * Attention, dans le fichier les indices commencent à 1 !
	 * @param str la chaine équivalente à un nombre
	 * @return un entier correspondant à un indice de tableau
	 */
	private static Integer toIndex(String str) {
		return Integer.parseInt(str)-1;
	}
	
	/**
	 * Retourne un ensemble d'entier sous une chaine de caractères formatée
	 * @param i les entiers à formater
	 * @return une chaine de caractères d'entier
	 */
	private static String format(int... i) {
		String format = "";
		for(int n : i) {
			format += n + " ";
		}
		return format.trim();
	}
	
	/**
	 * Retourne un ensemble de double sous une chaine de caractères formatée
	 * @param d les doubles à formater
	 * @return une chaine de caractères de doubles
	 */
	private static String format(double... d) {
		String format = "";
		for(double n : d) {
			format += n + " ";
		}
		return format.trim();
	}
	
	/**
	 * Fabrique une figure à partir d'un fichier
	 * @param file le fichier permettant de créer une figure
	 * @return une figure géométrique
	 * @throws IOException le fichier est introuvable
	 * @throws NoSuchElementException le fichier n'est pas un fichier de figure
	 */
	public static Model loadModel(File file) throws IOException, NoSuchElementException {
		Scanner s = new Scanner(file);
		Point3D[] pts = new Point3D[Integer.parseInt(s.next())];
		Segment[] seg = new Segment[Integer.parseInt(s.next())];
		Face[] fc = new Face[Integer.parseInt(s.next())];
		String str;
		int idxP = 0, idxS = 0, idxF = 0;
		
		while (s.hasNext()) {
			str = s.next();
			if (idxP < pts.length)
				pts[idxP++] = new Point3D(Double.parseDouble(str), Double.parseDouble(s.next()), Double.parseDouble(s.next()));
			else if (idxS < seg.length)
				seg[idxS++] = new Segment(pts[toIndex(str)], pts[toIndex(s.next())]);
			else if (idxF < fc.length)
				fc[idxF++] = new Face(seg[toIndex(str)], seg[toIndex(s.next())], seg[toIndex(s.next())]);
		}
		s.close();
		return new Model(fc,seg,pts);
	}
	
	/**
	 * Fabrique un fichier à partir d'une figure
	 * @param file le fichier dans lequel sera stocké la figure
	 * @param figure la figure géométrique à stocker dans le fichier
	 * @throws IOException cas où le fichier subit une erreur
	 */
	public static void saveModel(File file, Model figure) throws IOException {
		PrintWriter out = new PrintWriter(file + ".gts");
		int plength = figure.getVertices().length;
		int slength = figure.getSegments().length;
		int flength = figure.getFaces().length;
		out.println(format(plength,slength,flength));
		for(Point3D p : figure.getVertices()) {
			out.println(format(p.getX(),p.getY(),p.getZ()));
		}
		for(Segment s : figure.getSegments()) {
			out.println(format(s.getA().getNumero(),s.getB().getNumero()));
		}
		for(Face f : figure.getFaces()) {
			out.println(format(f.getSegment(0).getNumero(),
					f.getSegment(1).getNumero(), f.getSegment(2).getNumero()));
		}
		out.close();
	}

}
