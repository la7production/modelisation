/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.gui.scene;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import fr.la7prod.paintlib.model.Model;

/**
 * View est la vue de l'utilisateur pour voir une figure.
 * Elle est est définie par :
 * - un arrière plan (paysage)
 * - une figure au premier plan
 * - une source lumineuse déplacable qui éclaire la figure
 * - un contrôleur qui indique à la vue quand la figure change d'état
 */
public class View extends JPanel implements Observer {

	private static final long serialVersionUID = 502309143170322557L;
	
	private Color background;
	private Model figure;
	private SpotLight spot;
	private ViewController controller;
	private boolean newfigure;
	
	public View(Model figure, SpotLight spot) {
		this.background = Color.WHITE;
		this.setFigure(figure);
		this.setSpotLight(spot);
	}
	
	/**
	 * Retourne la figure observée par la vue
	 * @return la figure observée par la vue
	 */
	public Model getFigure() {
		return this.figure;
	}
	
	/**
	 * Retourne la source lumineuse contenue dans la vue
	 * @return la source lumineuse contenue dans la vue
	 */
	public SpotLight getSpotLight() {
		return this.spot;
	}
	
	/**
	 * Retourne le contrôleur de la vue qui permet de joindre la vue à sa figure
	 * @return le contrôleur de la vue joignant la vue et sa figure
	 */
	public ViewController getViewController() {
		return this.controller;
	}
	
	/**
	 * Retourne la couleur de l'arrière plan de la vue
	 * @return la couleur du fond de la vue
	 */
	public Color getBackground() {
		return this.background;
	}
	
	/**
	 * Saisie le contrôleur de la vue qui envoie les informations
	 * relatives à la figure, quand celle-ci subit des modifications d'état
	 * @param controller le contrôleur de la vue
	 */
	public void setController(ViewController controller) {
		if (this.controller != null) {
			this.controller.removeLink(this);
		}
		this.controller = controller;
		this.controller.linkTo(this);
	}
	
	/**
	 * Saisie la figure qui sera affichée dans la vue.
	 * Lors de son premier affichage, la figure est située au centre de la vue.
	 * @param figure la figure qui sera visible pour l'utilisateur
	 */
	public void setFigure(Model figure) {
		if (this.figure != null && this.figure.countObservers() > 0)
			this.figure.deleteObserver(this);
		this.figure = figure;
		this.figure.zoom(40);
		this.figure.addObserver(this);
		this.newfigure = true;
	}
	
	/**
	 * Saisie la source lumineuse qui éclairera la figure.
	 * @param spot la source lumineuse qui gère l'éclairage de la figure
	 */
	public void setSpotLight(SpotLight spot) {
		if (this.spot != null && this.spot.countObservers() > 0)
			this.spot.deleteObserver(this);
		this.spot = spot;
		this.spot.addObserver(this);
	}
	
	/**
	 * Saisie la couleur de l'arrière plan de la vue.
	 */
	public void setBackground(Color background) {
		this.background = background;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		if (newfigure) {
			figure.move(this.getWidth()/2, this.getHeight()/2);
			newfigure = false;
		}
		else {
			g.setColor(background);
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
			figure.draw(g, spot, true);
		}
	}

	@Override
	public void update(Observable obs, Object o) {
		this.repaint();
	}

}
