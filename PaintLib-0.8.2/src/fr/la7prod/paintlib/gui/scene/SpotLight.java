/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.gui.scene;

import java.awt.Color;

import fr.la7prod.paintlib.model.Barycentre;
import fr.la7prod.paintlib.model.Face;
import fr.la7prod.paintlib.model.Point3D;
import fr.la7prod.paintlib.model.util.Updatable;
import fr.la7prod.paintlib.model.util.Vector3D;

/**
 * SpotLight est une source lumineuse ajoutée à la scène.
 * Il y a alors des jeux d'ombres sur la figure.
 */
public class SpotLight extends Updatable {
	
	private Point3D point;
	private Color color;
	private double brightness;
	
	public SpotLight(Point3D point, Color color) {
		this.point = point;
		this.color = color;
		this.brightness = 0.00001;
	}
	
	/**
	 * Augmente la luminosité de la source lumineuse
	 */
	public void brighter() {
		this.brightness /= 2;
	}
	
	/**
	 * Diminue la luminosité de la source lumineuse
	 */
	public void darker() {
		this.brightness *= 2;
	}

	/**
	 * Retourne le point correspondant à la source lumineuse
	 * @return le point correspondant à la position du SpotLight
	 */
	public Point3D getPoint() {
		return point;
	}

	/**
	 * Modifie la position de la source lumineuse
	 * @param point le point correspondant à la nouvelle position du SpotLight
	 */
	public void setPoint(Point3D point) {
		this.point = point;
	}

	/**
	 * Retourne la couleur diffusée par la source lumineuse
	 * @return la couleur diffusée par le SpotLight
	 */
	public Color getColor() {
		return color;
	}
	
	/**
	 * Modifie la couleur diffusée par la source lumineuse
	 * @param color la nouvelle couleur du SpotLight
	 */
	public void setColor(Color color){
		this.color = color;
	}
	
	/**
	 * Retourne la couleur de la face selon le pourcentage de lumière sur elle
	 * On calcule donc un pourcentage qui correspond à la luminosité de la face selon une source lumineuse.
	 * 1- Pour cela, on récupère un vecteur normal de la face.
	 * 2- On récupère ensuite le vecteur qui va de la source lumineuse au barycentre de la face.
	 * 3- On calcule le produit sclaire du vecteur normal et du vecteur de la source.
	 * 4- Enfin on retourne le calcul du pourcentage de luminosité voulue.
	 * @param face la face qui subit le changement de lumière
	 */
	public void setColorOf(Face face) {
		//new PhongLight(this).setColorOf(face);
		Barycentre bary = face.getBarycentre();
		Vector3D nf = Vector3D.normal(face);
		Vector3D nl = new Vector3D(bary.getX()-point.getX(),bary.getY()-point.getY(),bary.getZ()-point.getZ());
		double cos = Vector3D.cos(nf,nl);
		double dist = nl.normalize();
		dist *= dist * brightness;
		
		face.setColor(new Color(
				(int)(Math.min(255,color.getRed() * cos / dist)),
				(int)(Math.min(255,color.getGreen() * cos / dist)),
				(int)(Math.min(255, color.getBlue() * cos / dist))));
	}
	
	@Override
	public String toString() {
		return "SpotLight[Color=" + color + ", Point=" + point + "]";
	}

}
