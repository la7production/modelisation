/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.gui.scene;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.la7prod.paintlib.gts.GtsCaller;
import fr.la7prod.paintlib.model.Model;
import fr.la7prod.paintlib.model.util.AutoRotate;

/**
 * ViewController est le contrôleur permettant d'associer une vue et une figure.
 * C'est le ViewController qui indique à la vue si la figure subit un changement d'état.
 * La vue s'adapte en conséquence et permet d'effectuer un repaint par exemple.
 */
public class ViewController implements MouseListener, MouseWheelListener, MouseMotionListener, KeyListener {
	
	protected Model figure;
	protected SpotLight spot;
	private AutoRotate thread;
	private List<View> views;
	
	public ViewController(Model figure, SpotLight spot) {
		this.figure = figure;
		this.spot = spot;
		this.thread = new AutoRotate(figure);
		this.views = new ArrayList<View>();
		this.thread.start();
	}
	
	public void linkTo(View view) {
		views.add(view);
		view.addMouseListener(this);
		view.addMouseWheelListener(this);
		view.addMouseMotionListener(this);
		view.addKeyListener(this);
	}
	
	public void removeLink(View view) {
		view.removeMouseListener(this);
		view.removeMouseWheelListener(this);
		view.removeMouseMotionListener(this);
		view.removeKeyListener(this);
		views.remove(view);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		figure.move(e.getX(),e.getY());
		figure.update();
		spot.update();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		spot.getPoint().move(e.getX(),e.getY());
		spot.update();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		double zoom = 0.9;
		if (e.getWheelRotation() < 0) {
			zoom = 1.1;
		}
		figure.zoom(zoom);
		figure.update();
		spot.update();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON2) {
			try {
				Model figure = new GtsCaller().open();
				this.figure = figure;
				this.thread.setModel(figure);
				for(View view : views)
					view.setFigure(figure);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		else if (e.getButton() == MouseEvent.BUTTON1) {
			spot.brighter();
			spot.update();
		}
		else if (e.getButton() == MouseEvent.BUTTON3) {
			spot.darker();
			spot.update();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	}

}
