/**
 * PaintLib est un programme sous licence GNU.
 * La licence GNU est fournie avec PaintLib.
 * 
 * @author Edouard CATTEZ - Lucas MOURA DE OLIVEIRA
 */
package fr.la7prod.paintlib.gui;

import java.awt.Color;
import java.io.IOException;
import java.util.NoSuchElementException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import fr.la7prod.paintlib.gts.GtsCaller;
import fr.la7prod.paintlib.gui.scene.SpotLight;
import fr.la7prod.paintlib.gui.scene.View;
import fr.la7prod.paintlib.gui.scene.ViewController;
import fr.la7prod.paintlib.model.Model;
import fr.la7prod.paintlib.model.Point3D;

/**
 * Launcher est le lanceur de l'application.
 * Il est la fenêtre qui contient tous les éléments du programme.
 */
public class Launcher extends JFrame {
	
	private static final long serialVersionUID = 8422299952523518385L;

	public Launcher() {
		this.setTitle("PaintLib 0.8 - The New Version - Kill all Bricout Alive Mother Fucker");
		this.setSize(800,800);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void start() {
		Model figure;
		SpotLight spot;
		View view;
		ViewController controller;
		Object options[] = { "Choisir une autre figure", "Annuler" };
		try {
			figure = new GtsCaller().open();
			spot = new SpotLight(new Point3D(0,0,0), Color.BLUE);
			view = new View(figure,spot);
			controller = new ViewController(figure,spot);
			view.setController(controller);
			this.setContentPane(view);
			this.setVisible(true);
		} catch(NullPointerException e) {
			System.exit(0);
		} catch(NoSuchElementException | IOException e) {
			int result = JOptionPane.showOptionDialog(this,
				    "Le fichier semble corrompu ou ne contient aucune figure",
				    "Erreur de chargement",
				    JOptionPane.YES_NO_CANCEL_OPTION,
				    JOptionPane.QUESTION_MESSAGE,
				    null,
				    options,
				    options[1]);
			if (result == 0)
				start();
			else
				System.exit(0);
		}
	}

	public static void main(String[] args) {
		new Launcher().start();
	}

}
