/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.main;

import javax.swing.JFrame;

import fr.la7prod.paintlib.scene.Scenes;

/**
 * Launcher est le lanceur de l'application.
 * Il est la fenêtre qui contient tous les éléments du programme.
 */
public class Launcher extends JFrame {
	
	private static final long serialVersionUID = 8422299952523518385L;

	public Launcher() {
		this.setTitle("PaintLib 0.8.3 - The New Version - Kill all Bricout Alive Mother Fucker");
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void start() {
		this.setContentPane(new Scenes());
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	public static void main(String[] args) {
		new Launcher().start();
	}

}
