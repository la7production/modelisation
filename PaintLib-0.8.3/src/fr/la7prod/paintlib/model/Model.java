/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.model;

import java.awt.Graphics;
import java.util.Arrays;

import fr.la7prod.paintlib.model.util.DrawingType;
import fr.la7prod.paintlib.model.util.Modelisable;
import fr.la7prod.paintlib.model.util.geometry.Vector3D;
import fr.la7prod.paintlib.scene.Light;

/**
 * Un model est un ensemble de faces, de segments et de points
 * avec son propre barycentre. C'est une forme géométrique qui peut
 * subir des mouvements (translation, rotation, zoom).
 * @see fr.la7prod.paintlib.model.util.Modelisable
 */
public class Model extends Modelisable {
	
	private Face[] faces;
	private Barycentre[] baries;
	private boolean bariesSorted;
	
	public Model(String name, Face[] faces, Segment[] segments, Point3D[] points) {
		super(name);
		int i = 0;
		this.setSegment(segments);
		this.setVertices(points);
		this.faces = faces;
		this.baries = new Barycentre[faces.length];
		for(Face f : faces) {
			baries[i++] = f.getBarycentre();
		}
		this.bariesSorted = false;
		this.getBarycentre().calculate();
	}
	
	/**
	 * Retourne toutes les faces constituant la figure
	 * @return le tableau de toutes les faces de la figure
	 */
	public Face[] getFaces() {
		return this.faces;
	}
	
	@Override
	public void rotate(Vector3D v) {
		super.rotate(v);
		bariesSorted = false;
	}
	
	@Override
	public void draw(Graphics g, Light spot, DrawingType type) {
		for(Barycentre b: baries) {
			b.calculate();
		}
		if (!bariesSorted) {
			Arrays.sort(baries);
			bariesSorted = true;
		}
		for(Barycentre b : baries) {
			b.getParent().draw(g, spot, type);
		}
	}
	
	@Override
	public String toString() {
		String str = "";
		for(Face f : faces)
			str += " " + f.toString() + "\n";
		return "Figure composée des faces:\n" + str;
	}

}
