/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.model;

import fr.la7prod.paintlib.model.util.Identifiable;
import fr.la7prod.paintlib.model.util.geometry.Coordinates;
import fr.la7prod.paintlib.model.util.geometry.Vector3D;

/**
 * Un point est un ensemble de coordonnées dans un espace en 3 dimensions
 * Il a également un numéro d'identifiant pour être reconnu par les segments
 * dans le fichier .gts
 */
public class Point3D extends Coordinates implements Cloneable, Identifiable {
	
	public static int NBINSTANCES = 0;
	public static int DPD = 50;
	
	private int id;
	
	public Point3D() {
		super(0,0,0);
	}
	
	public Point3D(double x, double y, double z) {
		super(x,y,z);
		this.id = ++NBINSTANCES;
	}
	
	@Override
	public int getNumero() {
		return this.id;
	}
	
	/**
	 * Transforme le point courant en vecteur
	 * @return un vecteur utilisant les coordonnées du point courant
	 */
	public Vector3D toVector() {
		return new Vector3D(this.getX(),this.getY(),this.getZ());
	}
	
	@Override
	public String toString() {
		return "Point de " + super.toString();
	}
	
	@Override
	public Point3D clone() {
		return new Point3D(this.getX(),this.getY(),this.getZ());
	}
	
}
