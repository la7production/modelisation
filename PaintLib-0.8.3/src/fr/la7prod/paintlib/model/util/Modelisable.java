/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.model.util;

import java.awt.Graphics;

import fr.la7prod.paintlib.model.Barycentre;
import fr.la7prod.paintlib.model.Point3D;
import fr.la7prod.paintlib.model.Segment;
import fr.la7prod.paintlib.model.util.geometry.Coordinates;
import fr.la7prod.paintlib.model.util.geometry.Movable;
import fr.la7prod.paintlib.model.util.geometry.Vector3D;
import fr.la7prod.paintlib.scene.Light;

/**
 * Modelisable indique qu'un objet est une figure géométrique
 * comme des modèles en 2D et 3D pouvant subir des modifications comme des mouvements.
 */
public abstract class Modelisable extends Updatable implements Movable {
	
	private String name;
	private Point3D[] vertices;
	private Segment[] segments;
	private Barycentre barycentre;
	
	public Modelisable(String name) {
		this.name = name;
		this.barycentre = new Barycentre(this);
	}
	
	/**
	 * Retourne le nom de l'objet modelisable
	 * @return le nom de l'objet modelisable sous forme d'une chaîne de caractères
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Saisie les segments qui définissent le modèle
	 * @param segments les segments contenus dans le modèle
	 */
	public void setSegment(Segment[] segments) {
		this.segments = segments;
	}
	
	/**
	 * Retourne tous les segments qui constituent le modèle
	 * @return un segment du tableau des segments du modèle
	 */
	public Segment[] getSegments() {
		return this.segments;
	}
	
	/**
	 * Saisie les sommets qui définissent le modèle
	 * @param vertices les sommets du modèle
	 */
	public void setVertices(Point3D[] vertices) {
		this.vertices = vertices;
	}
	
	/**
	 * Retourne tous les sommets qui constituent le modèle
	 * @return un sommet du tableau des sommets du modèle
	 */
	public Point3D[] getVertices() {
		return this.vertices;
	}
	
	/**
	 * Retourne un segment du modèle
	 * @param idx l'indice du segment
	 * @return un segment du tableau de segments du modèle
	 */
	public Segment getSegment(int idx) {
		if (idx < 0 || idx >= segments.length)
			throw new IndexOutOfBoundsException();
		return segments[idx];
	}
	
	/**
	 * Retourne un sommet du modèle
	 * @param idx l'indice du sommet
	 * @return un sommet du tableau de sommets du modèle
	 */
	public Point3D getVertex(int idx) {
		if (idx < 0 || idx >= vertices.length)
			throw new IndexOutOfBoundsException();
		return vertices[idx];
	}
	
	/**
	 * Retourne le barycentre du modèle
	 * @return le barycentre du modèle
	 */
	public Barycentre getBarycentre() {
		return this.barycentre;
	}
	
	@Override
	public void move(Coordinates c) {
		this.move(c.getX(), c.getY(), c.getZ());
	}
	
	@Override
	public void move(double x, double y) {
		this.move(x, y, barycentre.getZ());
	}
	
	@Override
	public void move(double x, double y, double z) {
		barycentre.translateModel(x, y, z);
	}
	
	@Override
	public void translate(Vector3D v) {
		for(Point3D p : this.getVertices()) {
			p.translate(v);
		}
		barycentre.translate(v);
		barycentre.save();
	}
	
	@Override
	public void zoom(double value) {
		for(Point3D p : this.getVertices()) {
			p.zoom(value);
		}
		barycentre.zoom(value);
		barycentre.restore();
	}
	
	@Override
	public void rotate(Vector3D v) {
		for(Point3D p : this.getVertices()) {
			p.rotate(v);
		}
		barycentre.rotate(v);
		barycentre.restore();
	}
	
	/**
	 * Dessine le modèle à l'écran
	 * @param g la zone de dessin
	 * @param spot la source lumineuse
	 * @param type indique si le modèle doit être dessiné pleinement ou en fil de fer
	 */
	public abstract void draw(Graphics g, Light spot, DrawingType type);

}
