/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.model.util.geometry;

/**
 * Un objet Movable est un objet pouvant subir différents des mouvements.
 * Dans notre cas, il est coupable à Modelisable pour créer des modèles
 */
public interface Movable {
	
	/**
	 * Déplace un objet movable en modifiant directement ses coordonnées
	 * @param c les nouvelles coordonnées de l'objet
	 */
	public void move(Coordinates c);
	
	/**
	 * Déplace un objet movable en modifiant directement ses coordonnées
	 * @param x la nouvelle coordonnée x
	 * @param y la nouvelle coordonnée y
	 */
	public void move(double x, double y);
	
	/**
	 * Déplace un objet movable en modifiant directement ses coordonnées
	 * @param x la nouvelle coordonnée x
	 * @param y la nouvelle coordonnée y
	 * @param z la nouvelle coordonnée z
	 */
	public void move(double x, double y, double z);
	
	/**
	 * Effectue une translation de l'objet movable
	 * @param v le vecteur de translation
	 */
	public void translate(Vector3D v);
	
	/**
	 * Effectue un agrandissement de l'objet movable
	 * Note: le barycentre de l'objet movable reste au même endroit,
	 * une translation est donc effectuée après que le zoom eut été fait.
	 * @param value le nombre de pixels d'agrandissement
	 */
	public void zoom(double value);
	
	/**
	 * Effectue la rotation des points d'un objet movable
	 * @param v l'angle pour la coordonnée x
	 */
	public void rotate(Vector3D v);

}
