/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.model.util;

import fr.la7prod.paintlib.model.util.geometry.Vector3D;

/**
 * AutoRotate permet d'effectuer une rotation
 * en continue de l'objet modelisable voulu.
 */
public class AutoRotate extends Thread {
	
	public static int SLEEP = 30;
	
	private Modelisable model;
	private boolean state;
	private double x;
	private double y;
	private double z;
	
	public AutoRotate(Modelisable model) {
		this.model = model;
		this.x = Math.PI/2;
		this.y = Math.PI/2;
		this.z = 0;
		super.start();
	}
	
	public synchronized boolean state() {
		return this.state;
	}
	
	public synchronized void start() {
		this.state = true;
		this.pause();
		this.notifyAll();
	}
	
	public synchronized void slumber() {
		this.state = false;
	}
	
	public synchronized void pause() {
		if (!state) {
			try {
				wait();
			} catch(InterruptedException e) {}
		}
		else {
			try {
				Thread.sleep(SLEEP);
			} catch(InterruptedException e) {}
		}
	}
	
	public void setModel(Modelisable model) {
		this.model = model;
	}
	
	public void incX() {
		this.x += Math.PI;
		if (x > Math.abs(Math.PI/2 + 4*Math.PI))
			x = -Math.PI;
	}
	
	public void incY() {
		this.y += Math.PI;
		if (y > Math.abs(Math.PI/2 + 4*Math.PI))
			y = -Math.PI;
	}
	
	public void incZ() {
		this.z += Math.PI;
		if (z > Math.abs(Math.PI/2 + 4*Math.PI))
			z = -Math.PI;
	}
	
	public void run(){
		while(true){
			this.pause();
			model.rotate(new Vector3D(x,y,z));
			model.update();
		}
	}
}