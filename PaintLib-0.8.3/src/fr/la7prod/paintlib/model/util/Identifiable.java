/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.model.util;

/**
 * Identifiable est une interface qui permet
 * d'implémenter un système de numérotation d'objet.
 * Il est utilisé par exemple pour Point3D et Segment afin de leur
 * donner des numéros d'identification qui seront utilisés dans les fichiers .gts
 */
public interface Identifiable {
	
	/**
	 * Retourne le numéro d'identifiant d'un objet identifiable
	 * @return un entier correspondant à l'identifiant d'un objet identifiable
	 */
	public int getNumero();

}
