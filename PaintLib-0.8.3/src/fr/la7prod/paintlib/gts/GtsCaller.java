/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.gts;

import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

import javax.swing.JFileChooser;

import fr.la7prod.paintlib.model.Model;

/**
 * GtsCaller permet d'afficher une boite de dialogue
 * afin de charger ou sauvegarder un fichier au format .gts
 */
public class GtsCaller {
	
	private static File currentDir;
	private JFileChooser chooser;
	
	public GtsCaller() {
		this.chooser = new JFileChooser();
		this.chooser.setFileFilter(new GtsFilter());
		this.chooser.setAcceptAllFileFilterUsed(false);
		this.chooser.setCurrentDirectory(currentDir);
	}
	
	/**
	 * Ouvre un fichier de figure via une boite de dialogue
	 * @return une figure géométrique
	 * @throws IOException le fichier est corrompu
	 * @throws NoSuchElementException le fichier n'est pas un fichier de figure
	 */
	public Model open() throws IOException, NoSuchElementException {
		File file;
		int result = chooser.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION) {
			file = chooser.getSelectedFile();
			currentDir = file.getParentFile();
			return GtsFactory.loadModel(file);
		}
		throw new NullPointerException();
	}
	
	/**
	 * Sauvegarde un fichier de figure via une boite de dialogue
	 * @param figure la figure géométrique à sauvegarder
	 * @throws IOException le fichier n'a pas pu être créé
	 */
	public void saveAs(Model figure) throws IOException {
		File file;
		int result = chooser.showSaveDialog(null);
		if (result == JFileChooser.APPROVE_OPTION) {
			file = chooser.getSelectedFile();
			currentDir = file.getParentFile();
			GtsFactory.saveModel(file, figure);
		}
	}

}
