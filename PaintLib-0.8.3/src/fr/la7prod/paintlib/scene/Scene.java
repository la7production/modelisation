/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import fr.la7prod.paintlib.model.Point3D;
import fr.la7prod.paintlib.model.util.AutoRotate;
import fr.la7prod.paintlib.model.util.DrawingType;
import fr.la7prod.paintlib.model.util.Modelisable;

/**
 * Une scène permet comme son nom l'indique, d'observer un model.
 * Elle est également composée d'une source lumineuse et il est possible
 * de lui attribuer un fond coloré. On peut aussi changer la façon de dessiner le model.
 */
public class Scene extends JPanel implements Observer {

	private static final long serialVersionUID = -8871512460715807565L;
	
	private Light light;
	private Modelisable model;
	private DrawingType type;
	private AutoRotate autorotate;
	private boolean ready;
	
	public Scene(Modelisable model, Light light) {
		this(model,light,Color.DARK_GRAY);
	}
	
	public Scene(Modelisable model, Light light, Color background) {
		this.setModel(model);
		this.light = light;
		this.light.addObserver(this);
		this.type = DrawingType.FILLED;
		this.setBackground(background);
		this.autorotate = new AutoRotate(model);
	}
	
	/**
	 * Associe un SceneManager à la scène
	 * @param sm le SceneManager a associer à la scène
	 */
	public void addSceneManager(SceneManager sm) {
		this.addKeyListener(sm);
		this.addMouseWheelListener(sm);
		this.addMouseMotionListener(sm);
	}
	
	/**
	 * Dissocie la scène d'un SceneManager
	 * @param sm le SceneManager a dissocier de la scène
	 */
	public void removeSceneManager(SceneManager sm) {
		if (sm.getAssociatedScene() == this) {
			this.removeKeyListener(sm);
			this.removeMouseWheelListener(sm);
			this.removeMouseMotionListener(sm);
		}
	}
	
	/**
	 * Retourne le model de la scène
	 * @return le model de la scène
	 */
	public Modelisable getModel() {
		return this.model;
	}
	
	/**
	 * Retourne la source lumineuse de la scène
	 * @return la source lumineuse de la scène
	 */
	public Light getLight() {
		return this.light;
	}
	
	/**
	 * Retourne le type de dessin pour le model
	 * @return le type de dessin pour le model
	 */
	public DrawingType getDrawingType() {
		return this.type;
	}
	
	/**
	 * Retourne le thread de rotation automatique associé à la scène
	 * @return le thread de rotation automatique associé à la scène
	 */
	public AutoRotate getAutoRotate() {
		return this.autorotate;
	}
	
	/**
	 * Saisie le model qui sera affichée dans la scène.
	 * Lors de son premier affichage, le model est située au centre de la scène.
	 * @param model le model qui sera visible pour l'utilisateur
	 */
	private void setModel(Modelisable model) {
		this.ready = false;
		if (this.model != null && this.model.countObservers() > 0)
			this.model.deleteObserver(this);
		this.model = model;
		this.model.addObserver(this);
	}
	
	/**
	 * Saisie le type de dessin du model
	 * @param type le type de dessin (filled,outlined,pointed)
	 */
	public void setDrawingType(DrawingType type) {
		this.type = type;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (ready)
			model.draw(g, light, type);
		else
			prepareModel();
	}
	
	/**
	 * Prépare le modèle pour l'affichage
	 */
	
	private void prepareModel() {
		Point3D[] vertices = model.getVertices();
		Point3D p;
		Point3D pxmin = vertices[0];
		Point3D pxmax = pxmin;
		Point3D pymin = pxmin;
		Point3D pymax = pxmin;
		double coeff;
		for(int i=1; i<vertices.length; i++) {
			p = vertices[i];
			if (p.getX() < pxmin.getX())
				pxmin = p;
			else if (p.getX() > pxmax.getX())
				pxmax = p;
			if (p.getY() < pymin.getY())
				pymin = p;
			else if (p.getY() > pymax.getY())
				pymax = p;
		}
		coeff = this.getWidth()/(pxmax.getX()-pxmin.getX());
		if (pxmax.getX()-pxmin.getX() < pymax.getY()-pymin.getY())
			coeff = this.getHeight()/(pymax.getY()-pymin.getY());
		model.move(this.getWidth()/2, this.getHeight()/2);
		model.zoom(coeff);
		while(pxmax.getX() > this.getWidth() || pxmin.getX() < 0
				|| pymin.getY() < 0 || pymax.getY() > this.getHeight()) {
			model.zoom(0.9);
		}
		ready = true;
		model.update();
	}

	@Override
	public void update(Observable obs, Object o) {
		this.repaint();
	}

}
