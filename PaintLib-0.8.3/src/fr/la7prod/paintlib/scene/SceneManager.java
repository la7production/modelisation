/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import fr.la7prod.paintlib.model.util.DrawingType;

/**
 * SceneManager est le contrôleur permettant d'associer une scene et un objet modelisable.
 * C'est le SceneManager qui indique à la scene si l'objet modelisable subit des changements d'état.
 * La scene s'adapte en conséquence et effectue un repaint par exemple.
 */
public class SceneManager implements KeyListener, MouseWheelListener, MouseMotionListener {
	
	private Scene scene;
	
	public SceneManager(Scene scene) {
		this.scene = scene;
	}
	
	/**
	 * Retourne la scène associée au SceneManager
	 * @return la scène associée au SceneManager
	 */
	public Scene getAssociatedScene() {
		return this.scene;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		char key = e.getKeyChar();
		if (key == 'r') {
			if (scene.getAutoRotate().state())
				scene.getAutoRotate().slumber();
			else
				scene.getAutoRotate().start();
		}
		else if (key == '+' || key == '-') {
			if (key == '+')
				scene.getLight().brighter();
			else
				scene.getLight().darker();
			scene.getLight().update();
		}
		else if (key == 'i' || key == 'o' || key == 'p') {
			if (key == 'i')
				scene.setDrawingType(DrawingType.FILLED);
			else if (key == 'o')
				scene.setDrawingType(DrawingType.OUTLINED);
			else
				scene.setDrawingType(DrawingType.POINTED);
			scene.getModel().update();
		}
		else if (key == 'x' || key == 'y' || key == 'z') {
			if (key == 'x')
				scene.getAutoRotate().incX();
			else if (key == 'y')
				scene.getAutoRotate().incY();
			else
				scene.getAutoRotate().incZ();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		scene.getModel().move(e.getX(),e.getY());
		scene.getLight().getPoint().move(e.getX(),e.getY());
		scene.getModel().update();
		//scene.getLight().update();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		scene.getLight().getPoint().move(e.getX(),e.getY());
		scene.getLight().update();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		double zoom = Math.abs(-e.getPreciseWheelRotation()+0.1);
		scene.getModel().zoom(zoom);
		scene.getLight().getPoint().zoom(zoom);
		scene.getLight().getPoint().move(e.getX(), e.getY());
		scene.getModel().update();
		//scene.getLight().update();
	}

}
