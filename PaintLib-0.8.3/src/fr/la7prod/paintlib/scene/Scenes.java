/**
 * This file is part of PaintLib.
 *
 * PaintLib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PaintLib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.				 
 * 
 * You should have received a copy of the GNU General Public License
 * along with PaintLib.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Edouard CATTEZ <edouard.cattez@sfr.fr>
 */
package fr.la7prod.paintlib.scene;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import fr.la7prod.paintlib.gts.GtsCaller;
import fr.la7prod.paintlib.model.util.Modelisable;

/**
 * Scenes est un ensemble de scènes divisés en onglets
 * qui peuvent être ajoutés et ou supprimés, tous indépendemment des autres.
 * Chaque scène répond à son propre Model et à son propre SceneManager
 */
public class Scenes extends JTabbedPane implements MouseListener {
	
	private static final long serialVersionUID = -2815798337429338667L;
	
	private Scene currentScene;

	public Scenes() {
		this.setPreferredSize(new Dimension(700,700));
		this.setFocusable(false);
		this.addTab("+",new JPanel());
		this.addMouseListener(this);
	}
	
	public void addTab(String title, Scene scene) {
		super.addTab(title, scene);
		this.setSelectedComponent(scene);
	}
	
	public void setSelectedComponent(Scene scene) {
		if (scene != null) {
			super.setSelectedComponent(scene);
			this.currentScene = scene;
			this.currentScene.requestFocusInWindow();
		}
	}
	
	@Override
	public void setSelectedIndex(int index) {
		super.setSelectedIndex(index);
		if (this.getSelectedComponent() instanceof Scene) {
			this.currentScene = (Scene)this.getSelectedComponent();
			this.currentScene.requestFocusInWindow();
		}
	}
	
	public void removeTabAt(int index) {
		super.removeTabAt(index);
		if (this.getSelectedIndex() > 0)
			currentScene = (Scene)this.getSelectedComponent();
		else
			currentScene = null;
	}
	
	public void remove(Scene scene) {
		super.remove(scene);
		if (this.getSelectedIndex() > 0)
			currentScene = (Scene)this.getSelectedComponent();
		else
			currentScene = null;
	}
	
	public Scene getCurrentScene() {
		return currentScene;
	}
	
	public Scene getScene(int index) {
		if (index > 0 && index < getTabCount())
			return (Scene)this.getComponentAt(index);
		return null;
	}
	
	public void addScene(Scene scene) {
		boolean found = false;
		int i = 1;
		int n = 0;
		while (i<this.getTabCount() && !found) {
			found = getScene(i).getModel().getName().equals(scene.getModel().getName());
			n = i;
			i++;
		}
		if (!found) {
			this.addTab(scene.getModel().getName(), scene);
			this.setTabComponentAt(this.getSelectedIndex(),new SceneTabCloser(this,scene));
		}
		else {
			this.setSelectedIndex(n);
		}
	}
	
	private void openModel() {
		try {
			Modelisable model = new GtsCaller().open();
			Scene scene = new Scene(model, new Light(Color.ORANGE));
			scene.addSceneManager(new SceneManager(scene));
			addScene(scene);
		} catch (NumberFormatException | IOException e) {
			Object options[] = { "Choisir une autre model", "Annuler" };
			int result = JOptionPane.showOptionDialog(this,
				    "Le fichier semble corrompu ou ne contient aucune model",
				    "Erreur de chargement",
				    JOptionPane.YES_NO_CANCEL_OPTION,
				    JOptionPane.QUESTION_MESSAGE,
				    null,
				    options,
				    options[1]);
			if (result == JOptionPane.YES_OPTION)
				openModel();
			else
				this.setSelectedComponent(currentScene);
		} catch(NullPointerException e) {
			this.setSelectedComponent(currentScene);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (this.getSelectedIndex() == 0) {
			this.setSelectedComponent(currentScene);
			openModel();
		}
		else {
			this.currentScene = (Scene)this.getSelectedComponent();
			this.currentScene.requestFocusInWindow();
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}

}
